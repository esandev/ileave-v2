<?php

class Membertodepart extends MY_Controller {

    public $page_title = 'ผู้อนุมัติประจำแผนก';

    function __construct() {
        parent::__construct();
        $this->load->model('db_m');
        $this->load->model('script_m');
        $this->load->model('membertodepart_m');

        $s_imeeting['activemenu'] = '003';
        $this->session->set_userdata('s_imeeting', $s_imeeting);
    }

    function index() {
        $data['page_title'] = $this->page_title;
        $data['content'] = 'membertodepart/todepart_v';
        $this->load->view('template_v', $data);
    }

    function ajax_get_grid($offset = 0) {
        $txtsearch = $this->input->post('txtsearch');

        $array = $this->membertodepart_m->get_all($txtsearch, $offset);
        $data['query'] = $array['query'];
        $data['pagination'] = $array['pagination'];
        $this->load->view('membertodepart/todepart_grid_v', $data);
    }

    function edit($member_id) {
        $this->db->where('id', $member_id);
        $row = $this->db->get('tbmember')->row_array();
        $data['row'] = $row;

        $data['page_title'] = $this->page_title;
        $data['content'] = 'membertodepart/todepart_frm_v';
        $this->load->view('template_v', $data);
    }

    //แสดงแผนกที่ยังไม่ได้เลือก และแผนกที่ได้เลือก
    function ajax_todepart() {
        $member_id = $this->input->post('member_id');

        //แผนกที่มีสิทธิ์อนุมัติ
        $sql = "SELECT tbmembertodepart.id, tbmembertodepart.member_id, tbmembertodepart.depart_id,";
        $sql.= " tbdepart.name FROM tbmembertodepart Inner Join tbdepart ON tbmembertodepart.depart_id = tbdepart.id";
        $sql.= " WHERE tbmembertodepart.member_id =  '$member_id' ORDER BY tbdepart.name ASC";
        $membertodepart = $this->db->query($sql)->result_array();

        //แผนกทั้งหมด
        $this->db->order_by('name');
        $query = $this->db->get('tbdepart')->result_array();

        // loop เพื่อเช็คว่าเหลือแผนกไหนที่ยังไม่มีสิทธิ์อนุมั้ติ
        $depart = array();
        foreach ($query as $r) {
            $this->db->where('member_id', $member_id);
            $this->db->where('depart_id', $r['id']);
            $count = $this->db->get('tbmembertodepart')->num_rows();
            if ($count == 0) {
                $depart[] = array(
                    'id' => $r['id'],
                    'name' => $r['name']
                );
            }
        }

        $data['todepart'] = $membertodepart;
        $data['depart'] = $depart;
        $this->load->view('membertodepart/todepart_check_grid_v', $data);
    }

    function ajax_save() {
        $member_id = $this->input->post('member_id');
        $data_array = $this->input->post('data_array');

        $count_data = count($data_array);
        for ($i = 0; $i < $count_data; $i++) {
            $this->db->where('member_id ', $member_id );
            $this->db->where('depart_id', $data_array[$i]);
            if (($this->db->get('tbmembertodepart')->num_rows() == 0)) {
                $data = array(
                    'member_id' => $member_id,
                    'depart_id' => $data_array[$i]
                );

                $this->db->insert('tbmembertodepart', $data);
            }
        }
        echo true;
    }

    function ajax_cancel() {
        $data_array = $this->input->post('data_array');
        $count_data = count($data_array);
        for ($i = 0; $i < $count_data; $i++) {
            $this->db->where('id', $data_array[$i]);
            $this->db->delete('tbmembertodepart');
        }
        echo true;
    }

}

?>
