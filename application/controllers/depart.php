<?php

class Depart extends MY_Controller {

    public $page_title = 'ข้อมูลแผนก';

    function __construct() {
        parent::__construct();
        $this->load->model('db_m');
        $this->load->model('script_m');
        $this->load->model('depart_m');

        $s_imeeting['activemenu'] = '002';
        $this->session->set_userdata('s_imeeting', $s_imeeting);
    }

    function index() {
        $this->db->order_by('name');
        $team = $this->db->get('tbteam')->result_array();
        $data['team'] = $team;

        //$data['extraHeadContent'] = $this->script_m->bootstrap_modal();
        $data['page_title'] = $this->page_title;
        $data['content'] = 'office/depart/depart_v';
        $this->load->view('office/template_v', $data);
    }

    function ajax_get_grid($offset = 0) {
        $txtsearch = $this->input->post('txtsearch');

        $array = $this->depart_m->get_all($txtsearch, $offset);
        $data['query'] = $array['query'];
        $data['pagination'] = $array['pagination'];
        $this->load->view('office/depart/depart_grid_v', $data);
    }

    function ajax_get_data() {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $row = $this->db->get('tbdepart')->row_array();

        echo json_encode($row);
    }

    function ajax_save() {
        $id = $this->input->post('id');

        $data = array(
            'code' => $this->input->post('code'),
            'name' => $this->input->post('name'),
            'team_id' => $this->input->post('team_id')
        );

        $result = $this->db_m->validate_save('tbdepart', $id, 'code', $this->input->post('code'), $data);

        echo json_encode($result);
    }

    function ajax_delete() {
        $this->db->where('id', $this->input->post('id'));
        $this->db->delete('tbdepart');
        echo true;
    }


}

?>
