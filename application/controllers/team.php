<?php

class Team extends MY_Controller {
    public $page_title = 'ข้อมูลหน่วยงาน';

    function __construct() {
        parent::__construct();
        $this->load->model('db_m');
        $this->load->model('script_m');
        $this->load->model('team_m');
        
        
    }

    function index() {
       // $data['extraHeadContent'] = $this->script_m->bootstrap_modal();
        $data['page_title'] = $this->page_title;
        $data['content'] = 'office/team/team_v';
        $this->load->view('office/template_v', $data);
    }

    function ajax_get_grid($offset = 0) {
        $data['page_title'] = $this->page_title;
        $txtsearch = $this->input->post('txtsearch');

        $array = $this->team_m->get_all($txtsearch, $offset);
        $data['query'] = $array['query'];
        $data['pagination'] = $array['pagination'];
        $this->load->view('office/team/team_grid_v', $data);
    }

    function ajax_get_data() {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $row = $this->db->get('tbteam')->row_array();

        echo json_encode($row);
    }

    function ajax_save() {
        $id = $this->input->post('id');

        $data = array(
            'code' => $this->input->post('code'),
            'name' => $this->input->post('name'),
        );

        $result = $this->db_m->validate_save('tbteam', $id, 'code', $this->input->post('code'), $data);

        echo json_encode($result);
    }

    function ajax_delete() {
        $this->db->where('id', $this->input->post('id'));
        $this->db->delete('tbteam');
        echo true;
    }

}

?>
