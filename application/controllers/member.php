<?php

class Member extends MY_Controller {

    public $page_title = 'ข้อมูลผู้ใช้งาน';

    function __construct() {
        parent::__construct();
        $this->load->model('db_m');
        $this->load->model('script_m');
        $this->load->model('member_m');
        $this->load->library('mydate');

//        $s_imeeting['activemenu'] = '003';
//        $this->session->set_userdata('s_imeeting', $s_imeeting);
    }

    function index() {
        $data['page_title'] = $this->page_title;
        $data['content'] = 'office/member/member_v';
        $this->load->view('office/template_v', $data);
    }

    function news() {
        $this->db->order_by('name');
        $data['depart'] = $this->db->get('tbdepart')->result_array();

        $data['page_title'] = $this->page_title . " (เพิ่มข้อมูล)";
        $data['content'] = 'office/member/member_frm_v';
        $this->load->view('office/template_v', $data);
    }

    function edit($id, $success = '0') {
        $this->db->where('id', $id);
        $row = $this->db->get('tbmember')->row_array();
        $data['row'] = $row;
        if ($success == '1') {
            $data['msg_text'] = 'บันทึกข้อมูลเสร็จเรียบร้อย';
        }

        $this->db->order_by('name');
        $data['depart'] = $this->db->get('tbdepart')->result_array();

        $data['page_title'] = $this->page_title;
        $data['content'] = 'office/member/member_frm_v';
        $this->load->view('office/template_v', $data);
    }

    function ajax_validate() {
        $id = trim($_POST['id']);
        $usename = trim($_POST['username']);

        $data['msg'] = '0';
        if ($this->db_m->validate('tbmember', $id, 'username', $usename)) {
            $data['msg'] = '1';
            $data['msg_text'] = 'ชื่อเข้าใช้งานนี้มีแล้ว ไม่สามารถสร้างซ้ำได้';
        }
        echo json_encode($data);
    }

    function save() {
        $id = $this->input->post('id');
        $password = $this->input->post('password1');

        $data = array(
            'name' => $this->input->post('name'),
            'position' => $this->input->post('position'),
            'username' => $this->input->post('username'),
            'mobile' => $this->input->post('mobile'),
            'tel' => $this->input->post('tel'),
            'depart_id' => $this->input->post('depart_id'),
            'status' => $this->input->post('status'),
            'mem_type' => $this->input->post('mem_type'),
            'email' => $this->input->post('email'),
            'birthday' => $this->mydate->dateToMysql($this->input->post('birthday')),
            'date_serve' => $this->mydate->dateToMysql($this->input->post('date_serve'))
        );

        if ($password != '') {
            $this->load->library('encrypt');
            $data['password'] = $this->encrypt->encode($password);
        }

        if ($id == '') {
            $this->db->insert('tbmember', $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            $this->db->update('tbmember', $data);
        }
        //redirect("member/edit/$id/1");
        redirect('member');
    }

    function ajax_get_grid($offset = 0) {
        $txtsearch = $this->input->post('txtsearch');

        $array = $this->member_m->get_all($txtsearch, $offset);
        $data['query'] = $array['query'];
        $data['pagination'] = $array['pagination'];
        $data['member_m'] = $this->member_m;
        $this->load->view('office/member/member_grid_v', $data);
    }

    function ajax_delete() {
        $this->db->where('id', $this->input->post('id'));
        $this->db->delete('tbmember');
        echo true;
    }

    //เปลี่ยนรหัสผ่านใหม่
    function changePass() {
        $data['page_title'] = $this->page_title."เปลี่ยนรหัสผ่าน";
        $data['content'] = 'office/member/changepass_v';
        $this->load->view('office/template_v', $data);
    }

    function changePassSave() {
        $this->load->library('encrypt');

        $old_pass = $this->input->post('old_pass');
        $new_pass = $this->input->post('new_pass');
        $con_pass = $this->input->post('con_pass');

        $s_login = $this->session->userdata('s_login');
        $this->db->where('id', $s_login['login_id']);
        $row = $this->db->get('tbmember')->row_array();

        if ($old_pass != $this->encrypt->decode($row['password'])) {
            $data['type_title'] = "error";
            $data['msg_text'] = "รหัสผ่านปัจจุบันไม่ถูกต้อง";
        } else {
             $data['type_title'] = "success";
            $data['msg_text'] = "เปลี่ยนรหัสผ่านเรียบร้อย";
            $member['password'] = $this->encrypt->encode($new_pass);
            $this->db->where('id', $s_login['login_id']);
            $this->db->update('tbmember', $member);
        }

        $data['content'] = 'office/member/changepass_v';
        $this->load->view('office/template_v', $data);
    }

}

?>
