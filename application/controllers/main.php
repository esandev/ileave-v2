<?php

class Main extends MY_Controller {

    var $page_title = '';

    function __construct() {
        parent::__construct();
        $this->load->model('leave_m');
        $this->load->library('mydate');
        $this->load->model('db_m');
    }

    function index($success = '0') {

        $data['page_title'] = "Office iLeave";


        $totalwait = $this->leave_m->totalwait();
        $data['totalApprove'] = $this->leave_m->totalApprove();
        $data['totaldisapproval'] = $this->leave_m->totaldisapproval();
        $data['totalwait'] = $totalwait;

        //แจ้งเตือนการทำรายการเมื่อทำการลาสำเร็จ
        if ($success == '1') {
            $data['msg_type_save'] = "success";
            $data['msg_text_save'] = "ทำรายการลาเสร็จเรียบร้อยแล้ว";
        }
//แจ้งเตือนรายการที่รออนุมัติ 
        if (!empty($totalwait)) {
            $data['msg_type'] = "warning";
            $data['msg_text'] = "คุณมีรายการ ลารออนุมัติ " . $this->leave_m->totalwait() . " รายการ";
        }


        $data['content'] = 'office/content_v';
        $this->load->view('office/template_v', $data);
    }

    //ดึงรายการลาที่อนุมัติแล้ว ไปแสดงหน้า content_v
    function approve_ajax_get_grid($offset = 0) {
        $data['page_title'] = $this->page_title;

        $array = $this->leave_m->get_approve($offset);
        $data['query'] = $array['query'];


        $data['pagination'] = $array['pagination'];
        $this->load->view('office/leave/approve_grid_v', $data);
    }

    //ดึงรายการลาที่รออนุมัติ ไปแสดงหน้า content_v
    function wait_ajax_get_grid($offset = 0) {
        $data['page_title'] = $this->page_title;

        $array = $this->leave_m->get_wait($offset);
        $data['query'] = $array['query'];


        $data['pagination'] = $array['pagination'];
        $this->load->view('office/leave/wait_grid_v', $data);
    }

    //ดึงรายการลาที่ไม่อนุมัติ ไปแสดงหน้า content_v
    function disapproval_ajax_get_grid($offset = 0) {
        $data['page_title'] = $this->page_title;

        $array = $this->leave_m->get_disapproval($offset);
        $data['query'] = $array['query'];


        $data['pagination'] = $array['pagination'];
        $this->load->view('office/leave/disapproval_grid_v', $data);
    }

    //เรียกรายการ เพื่อเปลี่ยนสถานะ
    function ajax_get_data() {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $row = $this->db->get('tbleavemanage')->row_array();

        echo json_encode($row);
    }

    function ajax_save() {
        $id = $this->input->post('id');

        $data = array(
            'status' => $this->input->post('status'),
        );

        $result = $this->db_m->validate_save('tbleavemanage', $id, 'id', $this->input->post('id'), $data);

        echo json_encode($result);
    }

    function ajax_delete() {
        $this->db->where('id', $this->input->post('id'));
        $this->db->delete('tbleavemanage');

        echo true;
    }

}

?>
