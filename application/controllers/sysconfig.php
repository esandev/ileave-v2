<?php

class Sysconfig extends MY_Controller {

    var $page_title = "ตั้งค่าการส่งอีเมลล์";

    function __construct() {
        parent::__construct();
        
    }

    function index($id=0,$success=0) {
        if ($id > 0) {
            $this->db->where('id', $id);
            $data['row'] = $this->db->get('tbconfig')->row_array();
        } else {
            $data['row'] = $this->db->get('tbconfig')->row_array();
        }
        
        if($success=='1'){
            $data['msg_text'] = 'บันทึกข้อมูลสำเร็จ';
        }

        $data['page_title'] = $this->page_title;
        $data['content'] = 'office/sysconfig/sysconfig_v';
        $this->load->view('office/template_v', $data);
    }

    function save() {
        $id = $this->input->post('id');
        $sendemail = $this->input->post('sendemail');
        $data = array(
            'sendemail' => ($sendemail == 'on') ? '1' : '0',
            'email' => $this->input->post('email'),
            'email_password' => $this->input->post('email_password'),
            'email2' => $this->input->post('email2')
        );

        if ($id == '') {
            $this->db->insert('tbconfig', $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            $this->db->update('tbconfig', $data);
        }

        redirect("sysconfig/index/$id/1");
    }

    function ajax_test_sendmail() {
        $message = "<b>ส่งเมลล์สำเร็จ 2</b>";

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => $this->input->post('email'), // Gmail username,
            'smtp_pass' => $this->input->post('email_password') //Gmail Password
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n"); /* for some reason it is needed */

        $this->email->from($this->input->post('email'), 'ระบบการลาออนไลน์ iLeave2.0');
        $this->email->to($this->input->post('email2'));
        $this->email->subject('ทดสอบจาก ระบบการลาออนไลน์ iLeave2.0');
        $this->email->message($message);
        
        if($this->email->send()){
            $msg = "สามารถส่ง Mail ได้";
        }else{
            $msg = $this->email->print_debugger();
        }

        $data['msg'] = $msg;
        echo json_encode($data);
    }

}

?>
