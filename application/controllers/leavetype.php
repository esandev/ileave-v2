<?php

class leavetype extends MY_Controller {

    public $page_title = 'ข้อมูลประเภทการลา';

    function __construct() {
        parent::__construct();
        $this->load->model('db_m');
        $this->load->model('script_m');
        $this->load->model('leavetype_m');

       
    }

    function index() {
        $this->db->order_by('name');
        $leavetype = $this->db->get('tbleave_type')->result_array();
        $data['leavetype'] = $leavetype;

        //$data['extraHeadContent'] = $this->script_m->bootstrap_modal();
        $data['page_title'] = $this->page_title;
        $data['content'] = 'office/leavetype/leavetype_v';
        $this->load->view('office/template_v', $data);
    }

    function ajax_get_grid($offset = 0) {
        $txtsearch = $this->input->post('txtsearch');

        $array = $this->leavetype_m->get_all($txtsearch, $offset);
        $data['query'] = $array['query'];
        $data['pagination'] = $array['pagination'];
        $this->load->view('office/leavetype/leavetype_grid_v', $data);
    }

    function ajax_get_data() {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $row = $this->db->get('tbleave_type')->row_array();

        echo json_encode($row);
    }

    function ajax_save() {
        $id = $this->input->post('id');

        $data = array(
            'code' => $this->input->post('code'),
            'name' => $this->input->post('name'),
            'url' => $this->input->post('url'),
            'description' => $this->input->post('description')
        );

        $result = $this->db_m->validate_save('tbleave_type', $id, 'code', $this->input->post('code'), $data);

        echo json_encode($result);
    }

    function ajax_delete() {
        $this->db->where('id', $this->input->post('id'));
        $this->db->delete('tbleave_type');
        echo true;
    }

}