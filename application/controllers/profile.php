<?php

class Profile extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('mydate');
        $this->load->library('myfile');
       
    }

    function index($success = '0') {
        $s_login = $this->session->userdata("s_login");
        $member_id = $s_login['login_id'];

        $this->db->where('id', $member_id);
        $row = $this->db->get('tbmember')->row_array();
        $data['row'] = $row;
        if ($success == '1') {
            $data['msg_text'] = 'บันทึกข้อมูเสร็จเรียบร้อย';
        }

        $this->db->order_by('name', 'asc');
        $data['depart'] = $this->db->get('tbdepart')->result_array();

        $data['page_title'] = "ข้อมูลส่วนตัว";
        $data['content'] = 'office/profile/profile_frm_v';
        $this->load->view('office/template_v', $data);
    }

    function save() {
        $id = $this->input->post('id');
        $password = $this->input->post('password1');

        $data = array(
            'name' => $this->input->post('name'),
            'position' => $this->input->post('position'),
            'mobile' => $this->input->post('mobile'),
            'tel' => $this->input->post('tel'),
            'depart_id' => $this->input->post('depart_id'),
            'email' => $this->input->post('email'),
            'birthday'=> $this->mydate->dateToMysql($this->input->post('birthday'))
        );

        if ($password != '') {
            $this->load->library('encrypt');
            $data['password'] = $this->encrypt->encode($password);
        }
        
        //******** upload file ****************
        if (isset($_FILES['pic_file'])) {
            $file_temp = $_FILES['pic_file']['tmp_name'];

            $name = $_FILES['pic_file']['name'];

            // rename
            $ext_file = explode(".", $_FILES['pic_file']['name']);
            $ext_file = $ext_file[count($ext_file) - 1];

            $timeVar = strtotime(date("H:i:s"));
            $minutes = ((idate('H', $timeVar) * 60) + idate('i', $timeVar) + idate('s', $timeVar));
            $file_name = $minutes . "." . $ext_file;

            $full_path = $this->myfile->GetPhysicalFromURL() . 'assets/upload/member/';

            // upload here
            if ($name != "") {
                if (file_exists($full_path . $file_name)) {
                    //ตรวจสอบว่ามีไฟล์ตามรหัสนี้หรือยัง
                    unlink($full_path . $file_name);
                }
                move_uploaded_file($file_temp, 'assets/upload/member/' . $file_name);
                $data['picture'] = $file_name;
            }
        }

        if ($id == '') {
            $this->db->insert('tbmember', $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            $this->db->update('tbmember', $data);
        }
        redirect("profile/index/1");
    }

}

?>
