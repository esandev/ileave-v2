<?php

class Membertodepart_m extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all($txtsearch, $offset, $per_page = 20, $url = '/membertodepart/ajax_get_grid') {
        $sql = "SELECT tbmember.id, tbmember.name, tbmember.username, tbdepart.name as depart_name";
        $sql.= ", tbmember.status, tbmember.mem_type  FROM tbmember Left Join tbdepart ON tbmember.depart_id = tbdepart.id";
        $sql.= " Where tbmember.status='1' And tbmember.mem_type='2'";

        if ($txtsearch != '') {
            $sql.= " And (tbmember.name like '%$txtsearch%') or (tbmember.username like '%$txtsearch%')";
            $sql.= " or (tbdepart.name like '%$txtsearch%')";
        }
        $total = $this->db->query($sql)->num_rows();


        $sql.= "  Order by tbmember.name limit $offset, $per_page";
        $query = $this->db->query($sql)->result_array();

        $this->load->library('pagination');
        $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
        $data['query'] = $query;
        return $data;
    }

}

?>
