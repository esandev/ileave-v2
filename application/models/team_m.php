<?php
class Team_m extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function get_all($txtsearch, $offset, $per_page = 20, $url = '/team/ajax_get_grid') {
        if ($txtsearch != '') {
            $this->db->like('name',$txtsearch);
            $this->db->or_like('code',$txtsearch);
        }
        $total = $this->db->get('tbteam')->num_rows();

        if ($txtsearch != '') {
            $this->db->like('name',$txtsearch);
            $this->db->or_like('code',$txtsearch);
        }
        $this->db->limit($per_page, $offset);
        $this->db->order_by('code', 'asc');
        $query = $this->db->get('tbteam')->result_array();

        $this->load->library('pagination');
        $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
        $data['query'] = $query;
        return $data;
    }
}
?>
