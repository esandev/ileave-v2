<?php

class Email_m extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //ตรวจสอบก่อนว่าระบบให้ส่งเมลล์ได้หรือไม่
    function check_sendmail(){
        $this->db->where('sendemail','1');
        $r = $this->db->get('tbconfig')->num_rows();
        if($r==0){
            return false;
        }else{
            return true;
        }
    }
    
    function sendmail($from, $from_name, $to, $subject, $message, $member_id = '0') {
        $success = false;

        $row = $this->db->get('tbconfig')->row_array();
        if (isset($row['id'])) {
            if ($row['sendemail'] == '1') {
                $this->load->library('phpmailer');
                $mail = new PHPMailer();

                $mail->IsSMTP();
                $mail->SMTPAuth = true;                  // enable SMTP authentication
                $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
                $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                $mail->Port = 465;                   // set the SMTP port

                $mail->Username = $row['email'];  // GMAIL username
                $mail->Password = $row['email_password'];            // GMAIL password


                $mail->From = $from;
                $mail->FromName = $from_name;
                $mail->Subject = $subject;
                $mail->Body = $message;                      //HTML Body
                $mail->AltBody = $message; //Text Body
                $mail->WordWrap = 100; // set word wrap

                $mail->AddAddress($to);
                $mail->IsHTML(true); // send as HTML
                if ($mail->Send()) {
                    $success = true;
                } else {
                    $data = array(
                        'log' => 'ไม่สามารถส่งเมลล์ได้',
                        'log_from' => 'ระบบส่งเมลล์',
                        'cdate' => date("Y-m-d H:i:s"),
                        'member_id' => $member_id
                    );
                }
            }
        }

        return $success;
    }

    function sendmail_($from, $from_name, $to, $subject, $message, $member_id = '0') {
        $success = false;

        $row = $this->db->get('tbconfig')->row_array();
        if (isset($row['id'])) {
            if ($row['sendemail'] == '1') {
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'charset' => 'utf-8',
                    'mailtype' => 'html',
                    'smtp_user' => $row['email'], // Gmail username,
                    'smtp_pass' => $row['email_password'] //Gmail Password
                );

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n"); /* for some reason it is needed */

                $this->email->from($from, $from_name);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($message);

                if ($this->email->send()) {
                    $success;
                } else {
                    $data = array(
                        'log' => $this->email->print_debugger(),
                        'log_from' => 'ระบบส่งเมลล์',
                        'cdate' => date("Y-m-d H:i:s"),
                        'member_id' => $member_id
                    );
                }
            }
        }

        return $success;
    }

}

?>
