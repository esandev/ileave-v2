<?php

class Script_m extends CI_Model {

    function _construct() {
        parent::Model();
    }

    function calendar() {
        $script = "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/calendar/calendarTLE.js\"></script>\n";
        $script .= "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/calendar/calendar-setup.js\"></script>\n";
        $script .= "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/calendar/lang/calendar-th.js\"></script>\n";
        $script .= "<style type=\"text/css\"> @import url(\"" . base_url() . "assets/js/calendar/calendar-win2k-cold-1.css\"); </style>\n";
        return $script;
    }

    function textformat() {
        $script = "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/jquery.maskedinput.js\"></script>\n";
        return $script;
    }

    function textnumber() {
        $script = "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/jquery.alphanumeric.js\"></script>\n";
        return $script;
    }

    function formatCurrency() {
        $script = "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/formatCurrency.js\"></script>\n";
        return $script;
    }

    function modal_window() {
        $base_url = base_url();
        $script = <<<EOD
        <link rel="stylesheet" href="{$base_url}assets/js/modal-window/modal-window.css" type="text/css"/> \n
        <!--[if lte IE 6]><link href="{$base_url}assets/js/modal-window/modal-window-ie6.css" type="text/css" rel="stylesheet" /><![endif]-->
        <script type="text/javascript" src="{$base_url}assets/js/modal-window/modal-window.js"></script> \n
EOD;
        return $script;
    }

    function bootstrap_modal() {
        $script = "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/bootstrap/bootstrap-modal.js\"></script> \n";
        return $script;
    }

    function bootstrap_tab() {
        $script = "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/bootstrap/bootstrap-tab.js\"></script> \n";
        return $script;
    }

    function bootstrap_tooltip() {
        $script = "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/bootstrap/bootstrap-tooltip.js\"></script> \n";
        return $script;
    }

    function ajax_upload() {
        $script = "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/ajaxfileupload.js\"></script> \n";
        return $script;
    }

    function mytab() {
        $script = "<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"" . base_url() . "assets/js/mytab/tabstyle.css\"/>\n";
        $script.= "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/mytab/my_tab.js\"></script>\n";
        return $script;
    }

    function lightbox() {
        $script = "<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"" . base_url() . "assets/js/lightbox/jquery.lightbox-0.5.css\"/>\n";
        $script.= "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/lightbox/jquery.lightbox-0.5.js\"></script>\n";
        return $script;
    }

    function highchart() {
        $script = "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/highchart/highcharts.js\"></script>\n";
        $script.= "<script type=\"text/javascript\" src=\"" . base_url() . "assets/js/highchart/exporting.js\"></script>\n";
        return $script;
    }

}

?>
