<?php

class Member_m extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all($txtsearch, $offset, $per_page = 20, $url = '/member/ajax_get_grid') {
        $sql = "SELECT tbmember.id, tbmember.name, tbmember.username, tbdepart.name as depart_name";
        $sql.= ", tbmember.status, tbmember.mem_type  FROM tbmember Left Join tbdepart ON tbmember.depart_id = tbdepart.id";

        if ($txtsearch != '') {
            $sql.= " Where (tbmember.name like '%$txtsearch%') or (tbmember.username like '%$txtsearch%')";
            $sql.= " or (tbdepart.name like '%$txtsearch%')";
        }
        $total = $this->db->query($sql)->num_rows();


        $sql.= "  Order by tbmember.name limit $offset, $per_page";
        $query = $this->db->query($sql)->result_array();

        $this->load->library('pagination');
        $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
        $data['query'] = $query;
        return $data;
    }

    function display_status($status) {
        //0=รอการอนุมัติ,1=ใช้งาน,2=ระงับการใช้งาน
        $ret = '';
        switch ($status) {
            case '0':
                $ret = 'รออนุมัติ';
                break;
            case '1':
                $ret = 'ใช้งาน';
                break;
            case '2':
                $ret = 'ระงับการใช้งาน';
                break;
                ;
        }
        return $ret;
    }

    function display_memtype($memtype) {
        //ประเภทสมาชิก 0=ผู้ลา,1=ผู้ดูแลระบบ, 2=ผู้อนุมัติประจำแผนก
        $ret = '';
        switch ($memtype) {
            case '0':
                $ret = 'ผู้ทำรายการลา';
                break;
            case '1':
                $ret = 'ผู้ดูแลระบบ';
                break;
            case '2':
                $ret = 'ผู้อนุมัติประจำแผนก';
                break;
                ;
        }
        return $ret;
    }

}

?>
