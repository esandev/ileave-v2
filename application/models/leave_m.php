<?php

class leave_m extends CI_Model {

    var $tbname = 'tbleavemanage';

    function __construct() {
        parent::__construct();
    }

    //get รายการที่รออนุมัติ และแสดงหน้าละ 20 รายการ
    function get_all($field_name, $text_search, $login_id, $offset, $per_page = 20, $url = '/leavemanage/index') {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";
        $sql.= " WHERE tbleavemanage.person_id = '$login_id'  and tbleavemanage.status= 'รออนุมัติ'";
        if ($field_name != '' && $text_search != '') {
            $sql.=" Where $field_name like '%$text_search%'";
        }
        $total = $this->db->query($sql)->num_rows();

        $sql.= "  Order by Leave_id desc limit $offset, $per_page";
        $query = $this->db->query($sql)->result_array();

        $this->load->library('pagination');
        $data['page_links'] = $this->pagination->pagin($total, $url, $per_page);

        $data['query'] = $query;
        return $data;
    }

    //รายการที่อนุมัติ
    function getID($field_name, $text_search, $login_id, $offset, $per_page = 20, $url = '/leavemanage/index') {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";
        $sql.= " WHERE tbleavemanage.person_id = '$login_id' and tbleavemanage.status= 'อนุมัติ'";
        if ($field_name != '' && $text_search != '') {
            $sql.=" Where $field_name like '%$text_search%'";
        }
        $total = $this->db->query($sql)->num_rows();

        $sql.= "  Order by Leave_id desc limit $offset, $per_page";
        $query = $this->db->query($sql)->result_array();

        $this->load->library('pagination');
        $data['page_links'] = $this->pagination->pagin($total, $url, $per_page);

        $data['query'] = $query;
        return $data;
    }

    //รายการที่ไม่อนุมัติ
    function getIDNO($field_name, $text_search, $login_id, $offset, $per_page = 20, $url = '/leavemanage/index') {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";
        $sql.= " WHERE tbleavemanage.person_id = '$login_id' and tbleavemanage.status= 'ไม่อนุมัติ'";
        if ($field_name != '' && $text_search != '') {
            $sql.=" Where $field_name like '%$text_search%'";
        }
        $total = $this->db->query($sql)->num_rows();

        $sql.= "  Order by Leave_id desc limit $offset, $per_page";
        $query = $this->db->query($sql)->result_array();

        $this->load->library('pagination');
        $data['page_links'] = $this->pagination->pagin($total, $url, $per_page);

        $data['query'] = $query;
        return $data;
    }

    //แสดงในรายงาน report
    function select_all($offset, $per_page = 20, $url = '/report/index') {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,tbperson.group_id,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";


        $total = $this->db->query($sql)->num_rows();

        $sql.= "  Order by Leave_id limit $offset, $per_page";
        $query = $this->db->query($sql)->result_array();

        $this->load->library('pagination');
        $data['page_links'] = $this->pagination->pagin($total, $url, $per_page);

        $data['query'] = $query;
        return $data;
    }

//get ข้อมูลทุกรายการโดยกรองตามเงื่อนไข และแสดงหน้าละ 20 รายการ
    function get_all_wait($offset, $per_page = 20, $url = '/leavemanage/wait') {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork,tbperson.group_id, tbperson.title, tbperson.firstname,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";
        $sql.= " WHERE tbleavemanage.status= 'รออนุมัติ'";

        $total = $this->db->get('tbleavemanage')->num_rows();

        $sql.= "  Order by Leave_id desc limit $offset, $per_page";
        $query = $this->db->query($sql)->result_array();

        $this->load->library('pagination');
        $data['page_links'] = $this->pagination->pagin($total, $url, $per_page);

        $data['query'] = $query;
        return $data;
    }

//get total  Wait รายการรออนุมัติ
    function totalwait() {

        $s_login = $this->session->userdata('s_login');
        $member_id = $s_login['login_id'];
        $mem_type = $s_login['login_type'];

        if ($mem_type == '3' || $mem_type == '1' || $mem_type == '2'):

            $sql = "SELECT * FROM tbleavemanage WHERE status = 'wait' ";
            return $this->db->query($sql)->num_rows();

        else :
            $sql = "SELECT * FROM tbleavemanage WHERE status = 'wait' AND member_id = $member_id";
            return $this->db->query($sql)->num_rows();
        endif;
    }

    //get total  Wait รายการอนุมัติ
    function totalApprove() {
        $s_login = $this->session->userdata('s_login');
        $member_id = $s_login['login_id'];
        $mem_type = $s_login['login_type'];

        if ($mem_type == '3' || $mem_type == '1' || $mem_type == '2'):

            $sql = "SELECT * FROM tbleavemanage WHERE status = 'approve' ";
            return $this->db->query($sql)->num_rows();

        else :
            $sql = "SELECT * FROM tbleavemanage WHERE status = 'approve' AND member_id = $member_id";
            return $this->db->query($sql)->num_rows();
        endif;
    }

    //get total  Wait ไม่อนุมัติ/ยกเลิก
    function totaldisapproval() {

        $s_login = $this->session->userdata('s_login');
        $member_id = $s_login['login_id'];
        $mem_type = $s_login['login_type'];

        if ($mem_type == '3' || $mem_type == '1' || $mem_type == '2'):

            $sql = "SELECT * FROM tbleavemanage WHERE status = 'disapproval' ";
            return $this->db->query($sql)->num_rows();

        else :
            $sql = "SELECT * FROM tbleavemanage WHERE status = 'disapproval' AND member_id = $member_id";
            return $this->db->query($sql)->num_rows();
        endif;
    }

    //get total
    function totalMe($person_id) {

        $sql = "SELECT tbleavemanage.id AS Leave_id,tbleavemanage.person_id AS person_id , tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";
        $sql.= " WHERE tbleavemanage.status= 'wait' AND tbleavemanage.person_id = '$person_id'";

        return $this->db->query($sql)->num_rows();
    }

    //get total all
    function totalall() {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";

        return $this->db->query($sql)->num_rows();
    }

    //get total all
    function totalyes() {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";
        $sql.= " Where tbleavemanage.status = 'อนุมัติ'";
        return $this->db->query($sql)->num_rows();
    }

    //get total all
    function totalno() {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";
        $sql.= " Where tbleavemanage.status = 'ไม่อนุมัติ'";
        return $this->db->query($sql)->num_rows();
    }

    //get ข้อมูลทุกรายการโดยกรองตามเงื่อนไข และแสดงหน้าละ 20 รายการ
    function get_all_yes($offset, $per_page = 20, $url = '/leavemanage/index') {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,tbperson.group_id,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";
        $sql.= " WHERE tbleavemanage.status= 'อนุมัติ'";

        $total = $this->db->query($sql)->num_rows();

        $sql.= "  Order by Leave_id limit $offset, $per_page";
        $query = $this->db->query($sql)->result_array();


        $data['query'] = $query;
        return $data;
    }

    //get ข้อมูลทุกรายการโดยกรองตามเงื่อนไข และแสดงหน้าละ 20 รายการ
    function get_all_no($offset, $per_page = 20, $url = '/leavemanage/index') {

        $sql = "SELECT tbleavemanage.id AS Leave_id, tbleavemanage.datefrom,tbleavemanage.dateto, tbleavemanage.amountdate,tbleavemanage.comment,tbleavemanage.status,tbleavemanage.dateregist,";
        $sql.= "tbperson.id, tbperson.code,tbperson.pwork, tbperson.title, tbperson.firstname,tbperson.group_id,";
        $sql.= " tbperson.surname,tbperson.salary,tbcategory.name AS category_name";
        $sql.= " FROM tbleavemanage Inner Join tbperson ON tbleavemanage.person_id = tbperson.id";
        $sql.= " Inner Join tbcategory ON tbleavemanage.category_id = tbcategory.id";
        $sql.= " WHERE tbleavemanage.status= 'ไม่อนุมัติ'";

        $total = $this->db->query($sql)->num_rows();

        $sql.= "  Order by Leave_id limit $offset, $per_page";
        $query = $this->db->query($sql)->result_array();

        $data['query'] = $query;
        return $data;
    }

    function display_status($status) {
        //0=รอการอนุมัติ,1=ใช้งาน,2=ระงับการใช้งาน
        $ret = '';
        switch ($status) {

            case 'รออนุมัติ':
                $ret = "<img src='" . base_url() . "assets/images/toolbar/reload.png'/> " . $status;
                break;
            case 'อนุมัติ':
                $ret = "<img src='" . base_url() . "assets/images/toolbar/ok.png'/> " . $status;
                break;
            case 'ไม่อนุมัติ':
                $ret = "<img src='" . base_url() . "assets/images/toolbar/stop.png'/> " . $status;
                break;
                ;
            case 'ยกเลิก':
                $ret = "<img src='" . base_url() . "assets/images/toolbar/cancel.png'/> " . $status;
                break;
                ;
        }
        return $ret;
    }

    //ดึงรายการที่อนุมัติแล้ว
    function get_approve($offset, $per_page = 50, $url = '/main/approve_ajax_get_grid') {

        $s_login = $this->session->userdata('s_login');
        $member_id = $s_login['login_id'];
        $mem_type = $s_login['login_type'];


        if ($mem_type == '3' || $mem_type == '1' || $mem_type == '2'):
            $total = $this->db->get('tbleavemanage')->num_rows();

            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('tbleavemanage', array('status' => 'approve'))->result_array();

            $this->load->library('pagination');
            $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
            $data['query'] = $query;
            return $data;

        else :

            $total = $this->db->get('tbleavemanage')->num_rows();


            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');


            $query = $this->db->get_where('tbleavemanage', array('member_id' => $member_id, 'status' => 'approve'))->result_array();

            $this->load->library('pagination');
            $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
            $data['query'] = $query;
            return $data;
        endif;
    }

    //ดึงรายการที่รออนุมัติ
    function get_wait($offset, $per_page = 50, $url = '/main/wait_ajax_get_grid') {

        $s_login = $this->session->userdata('s_login');
        $member_id = $s_login['login_id'];
        $mem_type = $s_login['login_type'];


        //ถ้าเป็น admin จะเห็นรายการทั้งหมด
        if ($mem_type == '3' || $mem_type == '1' || $mem_type == '2'):

            $total = $this->db->get('tbleavemanage')->num_rows();
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('tbleavemanage', array('status' => 'wait'))->result_array();

            $this->load->library('pagination');
            $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
            $data['query'] = $query;
            return $data;

        else :
//เห็นแค่สมาชิก
            $total = $this->db->get('tbleavemanage')->num_rows();
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('tbleavemanage', array('member_id' => $member_id, 'status' => 'wait'))->result_array();

            $this->load->library('pagination');
            $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
            $data['query'] = $query;
            return $data;
        endif;
    }

    //ดึงรายการที่ไม่อนุมัติ
    function get_disapproval($offset, $per_page = 50, $url = '/main/disapproval_ajax_get_grid') {

        $s_login = $this->session->userdata('s_login');
        $member_id = $s_login['login_id'];
        $mem_type = $s_login['login_type'];


        //ถ้าเป็น admin จะเห็นรายการทั้งหมด
        if ($mem_type == '3' || $mem_type == '1' || $mem_type == '2'):

            $total = $this->db->get('tbleavemanage')->num_rows();
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('tbleavemanage', array('status' => 'disapproval'))->result_array();

            $this->load->library('pagination');
            $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
            $data['query'] = $query;
            return $data;

        else :
//เห็นแค่สมาชิก
            $total = $this->db->get('tbleavemanage')->num_rows();
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('tbleavemanage', array('member_id' => $member_id, 'status' => 'disapproval'))->result_array();

            $this->load->library('pagination');
            $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
            $data['query'] = $query;
            return $data;
        endif;
    }

}

?>