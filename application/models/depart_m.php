<?php
class Depart_m extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function get_all($txtsearch, $offset, $per_page = 20, $url = '/depart/ajax_get_grid') {
        $sql = "SELECT tbdepart.id, tbdepart.code, tbdepart.name, tbteam.name AS team_name";
        $sql.= " FROM tbteam Right Join tbdepart ON tbdepart.team_id = tbteam.id ";
        
        $sql2 = $sql;
        if ($txtsearch != '') {
            $sql.=" Where tbdepart.name like '%$txtsearch%' or tbdepart.code like '%$txtsearch%' or tbteam.name like '%$txtsearch%'";
        }
        $total = $this->db->query($sql)->num_rows();

        if ($txtsearch != '') {
            $sql2.=" Where tbdepart.name like '%$txtsearch%' or tbdepart.code like '%$txtsearch%' or tbteam.name like '%$txtsearch%'";
        }
        $sql2.=" ORDER BY tbdepart.code ASC LIMIT $offset,$per_page";
        $query = $this->db->query($sql2)->result_array();

        $this->load->library('pagination');
        $data['pagination'] = $this->pagination->pagin($total, $url, $per_page);
        $data['query'] = $query;
        return $data;
    }
}
?>
