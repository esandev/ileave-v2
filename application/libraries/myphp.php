<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myphp {
    public function  __construct() {

    }

    /*
     * Funciton สำหรับเอาค่าใส่ Drowdown
     * $name = ชื่อ และ id รับค่าแบบ String
     * $data = ชื่อ Field ที่ต้องการแสดง Value, ชื่อ Field ที่ต้องการแสดง ผล รับค่าแบบ Array เช่น $data['code'],$data['name']
     * $data = array ของข้อมูล เช่น $data['query']
     * $selected = ค่าที่ต้องการให้ Select รับค่าแบบ String
    */
    function set_dropdown($name="ddl",$data='',$selected=null) {
        $dd = '<select name="' . $name . '" id="' . $name . '" style="padding:2px;">';

        $selected = is_null($selected) ? $data['field_id'] : $selected;

        $data_id = $data['field_id'];
        $data_name = $data['field_name'];

        $dd.='<option value="">**** พิมพ์ชื่อที่ต้องการ ****</option>';
        foreach($data['query'] as $r) {
            $dd .= '<option value="' . $r[$data_id] . '"';
            if ($r['code'] == $selected) {
                $dd .= ' selected';
            }
            $dd.='>'.$r[$data_name].'&nbsp;&nbsp;</option>';
        }
        $dd .= '</select>';
        return $dd;
    }

    function timedropdown($name="time", $time_range=array(), $class='') {
        $dd = '<select name="' . $name . '" id="' . $name . '" class="'.$class.'">';
        /*         * * the current year ** */

        for ($i = $time_range[0]; $i <= $time_range[1]; $i++) {
            $dd .= '<option value="' . sprintf('%02d',$i) . '"';
            $dd .= '>' . sprintf('%02d',$i) . '&nbsp;&nbsp;</option>';
        }
        $dd .= '</select>';
        return $dd;
    }


}
