<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Login - iLeave 2.0</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">   

        <!-- Styles -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-overrides.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/slate.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/components/signin.css" rel="stylesheet" type="text/css">   

        <!-- Javascript -->

 
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.7.2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.8.18.custom.min.js"></script>    
        <script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/demos/signin.js"></script>
        <!-- validation -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/js/validation/css/screen.css"/>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validation/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validation/jquery.metadata.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validation/localization/messages_th.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.js"></script>
        <?php if ($msg != ''): ?>
            <script type="text/javascript">
                $(function() {
                    $('#div_alert_text').text("<?php echo $msg; ?>");
                    $('#div_alert').show();
                });
            </script>

        <?php endif; ?>
        <script type="text/javascript">
            $(function() {
                $('#alert_frm').css('display', 'none');
                $("#myform").validate();

                $('#password').bind('keypress', function(e) {
                    if (e.keyCode == 13) {
                        login();
                    }
                });
            });

            function login() {
                $('#myform').submit();
            }
        </script>

    </head>

    <body>

        <div class="account-container login">

            <div class="content clearfix">

                <?php echo form_open('login/login_check', array('id' => 'myform', 'style' => 'padding-bottom:0px;margin-bottom:0px')); ?>
                <input type="hidden" id="id" name="id" value="<?php echo @$row['id']; ?>"/>
                <h1>Sign In For iLeave  2.0</h1>		

                <div class="login-fields">
                    <div id="div_alert" class="alert alert-error hide">
                        <div id="div_alert_text"></div>
                    </div>
                    <div class="field">
                        <label for="username">Username:</label>
                        <input type="text" id="username" name="username" value="<?php echo @$row['username']; ?>" placeholder="Username" class="login username-field required" />
                    </div> <!-- /field -->

                    <div class="field">
                        <label for="password">Password:</label>
                        <input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field required"/>
                    </div> <!-- /password -->

                </div> <!-- /login-fields -->

                <div class="login-actions">


                    <button onclick="return login();" class="button btn btn-secondary btn-large">เข้าสู่ระบบ</button>

                </div> <!-- .actions -->


                <?php echo form_close(); ?>

            </div> <!-- /content -->

        </div> <!-- /account-container -->

    </body>
</html>
