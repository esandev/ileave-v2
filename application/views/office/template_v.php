<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Dashboard | iLeave 2.0</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">


        <!-- Styles -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-overrides.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/slate.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/slate-responsive.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/pages/dashboard.css" rel="stylesheet">

        <!-- Javascript -->

        <script src="<?php echo base_url(); ?>assets/js/jquery-1.7.2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/Slate.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>


        <script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.8.21.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>

        <link href="<?php echo base_url(); ?>assets/js/plugins/msgGrowl/css/msgGrowl.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/plugins/msgGrowl/js/msgGrowl.js"></script>



        <!-- validation -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/js/validation/css/screen.css"/>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validation/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validation/jquery.metadata.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validation/localization/messages_th.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.js"></script>

        <!-- facebox -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/facebox/facebox.css" type="text/css"/>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/facebox/facebox.js"></script>
        <link href="<?php echo base_url(); ?>assets/js/plugins/lightbox/themes/evolution-dark/jquery.lightbox.css" rel="stylesheet">  
        <script src="<?php echo base_url(); ?>assets/js/plugins/lightbox/jquery.lightbox.js"></script>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <?php
        if (isset($extraHeadContent)) {
            echo $extraHeadContent;
        }

        //ตรวจสอบการ login
        $s_login = $this->session->userdata('s_login');
        ?>
       
    </head>

    <body>
        <div id="header">
            <div class="container">			
                <h1><a href="<?php echo base_url(); ?>main">iLeave Admin 2.0</a></h1>			
                <div id="info">				
                    <a href="javascript:;" id="info-trigger">
                        <i class="icon-cog"></i>
                    </a>
                    <?php if (@$s_login['login_status'] == '1'): ?>
                        <div id="info-menu">

                            <div class="info-details">

                                <h4>Welcome,is <?php echo $s_login['login_code']; ?>.</h4>

                                <p>
                                    <i class="icon-off"></i> <?php echo anchor('login/logout', 'ออกจากระบบ'); ?>
                                    <br>

                                    มีข้อความใหม่ <a href="javascript:;">5 messages.</a>
                                </p>

                            </div> <!-- /.info-details -->
						<?php 	
						$pic = $this->db->get_where('tbmember',array('id'=>$s_login['login_id']))->result_array();
						foreach($pic as $r){
							if($r['picture']!=''){
						?>
                            <div class="info-avatar">

                                <img src="<?php echo base_url(); ?>assets/upload/member/<?php echo $r['picture'];?>" alt="avatar" width="65px" height ="65px">

                            </div> <!-- /.info-avatar -->
							<?php } } ?>
                        </div> <!-- /#info-menu -->
                    <?php endif; ?>
                </div> <!-- /#info -->

            </div> <!-- /.container -->

        </div> <!-- /#header -->


        <div id="nav">
            <?php $this->view('office/topmenu_v'); ?>

        </div> <!-- /#nav -->

        <div id="content">
            <?php $this->view($content); ?>
        </div> <!-- /#content -->
        <div id="footer">	
            <div class="container">
                &copy; 2014 ileave 2.0 by www.esandev.com, all rights reserved.
            </div> <!-- /.container -->		
        </div> <!-- /#footer -->
    </body>
</html>
