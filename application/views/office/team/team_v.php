<?php $this->view('ajax_loading_v'); ?>
<script type="text/javascript">
    $(function() {
        load_div_grid();

        $("div.pagination a").live("click", function(event) {
            var p = {};
            p['txtsearch'] = $('#txtsearch').val();

            event.preventDefault();
            var url = $(this).attr("href");
            if (url != undefined) {
                $('#div_grid').load(url, p);
            }
            return false;
        });


        $('#txtsearch').focus();
        $('#txtsearch').keydown(function(e) {
            if ((e.keyCode == 13)) {
                load_div_grid();
                return false;
            }
        });

        $("#myform").validate({
            submitHandler: function() {
                save();
            }
        });
    });

    function load_div_grid() {
        var p = {};
        p['txtsearch'] = $('#txtsearch').val();
        $('#div_grid').load("<?php echo site_url('team/ajax_get_grid') ?>", p);
        return false;
    }

    function save() {
        var p = {};
        p['id'] = $('#id').val();
        p['code'] = $('#code').val();
        p['name'] = $('#name').val();
        $.ajax({
            data: p,
            url: "<?php echo site_url('team/ajax_save') ?>",
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (data.msg == '1') {
                    //$.facebox(data.msg_text);
                    $('#alert_frm').text(data.msg_text).show();
                } else {
                    $('#myModal').modal('hide');
                    load_div_grid();
                }
            },
            error: function() {
                alert('ไม่สามารถทำรายการได้ !!');
            }
        });
        return false;
    }

    function grid_btn_edit(id) {
        clear_form();
        var p = {};
        p['id'] = id;
        $.ajax({
            data: p,
            url: "<?php echo site_url('team/ajax_get_data') ?>",
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                $('#id').val(data.id);
                $('#code').val(data.code);
                $('#name').val(data.name);

                $('#myModal').modal('show');
            },
            error: function() {
                alert('ไม่สามารถทำรายการได้ !!');
            }
        });
        return false;
    }

    function grid_btn_del(id) {
        if (confirm('คุณต้องการลบข้อมูลรายการนี้ ใช่หรือไม่?')) {
            var p = {};
            p['id'] = id;
            $.ajax({
                data: p,
                url: "<?php echo site_url('team/ajax_delete') ?>",
                type: 'POST',
                dataType: 'json',
                success: function() {
                    load_div_grid();
                },
                error: function() {
                    alert('ไม่สามารถทำรายการได้ !!');
                }
            });
        }
        return false;
    }

    function clear_form() {
        $('label.error,#alert_frm').css('display', 'none');
        $('input').removeClass('error');

        $('#id').val('');
        $('#code').val('');
        $('#name').val('');
        return false;
    }
</script>
<div class="container">
    <div class="widget-header">
        <h3>
            <i class="icon-th-list"></i> 
            <?php echo $page_title; ?>

        </h3>
        <div class="pull-right">
            <a href="#myModal" role="button" class="btn" data-toggle="modal" onclick="return clear_form();"><i class="icon-plus"></i> เพิ่มข้อมูล</a>
            <?php echo anchor('main', "<i class='icon-remove-circle'></i> ปิด", array('class' => 'btn')); ?>
        </div>

    </div>
    <div class="textbox_content">
        <div class="widget-header">
            <div class="widget-actions">
                <?php echo form_open('', array('class' => 'form-inline')); ?>

                <div class="input-append">
                    <input placeholder="ค้นหารายการ" onclick="return load_div_grid();"class="span4" id="txtsearch" name="txtsearch" type="text">

                </div>
                <?php echo form_close(); ?>
            </div> <!-- /.widget-actions -->		
        </div> <!-- /.widget-header -->

        <div id="div_grid"></div>
    </div>
</div>

<!-- หน้าจอเพิ่มข้อมูล -->
<div class="modal hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form method="post" class="form-horizontal" id="myform">
        <div class="modal-header" style="padding:10px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4><?php echo $page_title; ?></h4>
        </div>
        <div class="modal-body" style="padding:10px;">
            <div class="alert alert-error" style="display:none" id="alert_frm">
            </div>
            <input type="hidden" id="id"/>
            <table border="0" cellpadding="4" cellspacing="1" class="tblform">
                <tbody>
                    <tr>
                        <td width="100" class="td1">รหัสหน่วยงาน :</td>
                        <td>
                            <input type='text' id="code" name='code' class="span2" maxlength="20" required/>
                            <span class="required">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">ชื่อหน่วยงาน :</td>
                        <td>
                            <input type='text' id="name" name='name' class="input-xlarge" maxlength="120" required/>
                            <span class="required">*</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true"> ปิด </button>
            <button class="btn btn-info" type="submit"><i class="icon-hdd icon-white"></i> บันทึกข้อมูล</button>
        </div>
    </form>
</div>
