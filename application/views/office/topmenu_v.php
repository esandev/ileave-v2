<?php
$s_login = $this->session->userdata('s_login');
$login_type = $s_login['login_type'];
?>
<div class="container">

    <a href="javascript:;" class="btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <i class="icon-reorder"></i>
    </a>

    <div class="nav-collapse">

        <ul class="nav">

            <li class="nav-icon active">
                <a href="<?php echo base_url(); ?>main">
                    <i class="icon-home"></i>
                    <span>Home</span>
                </a>	    				
            </li>
            <?php if ($login_type == '3' || $login_type == '1' || $login_type == '2'): ?>
                <li class="dropdown">					
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-th"></i>
                        Sitting
                        <b class="caret"></b>
                    </a>	

                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>leavetype">จัดการประเภทการลา</a></li>
                        <li><a href="<?php echo base_url(); ?>team">จัดการหน่วยงาน/สาขา</a></li>
                        <li><a href="<?php echo base_url(); ?>depart">จัดการแผนก</a></li>
                        <li><a href="<?php echo base_url(); ?>member">ข้อมูลพนักงาน</a></li>
                    </ul>    				
                </li>
            <?php endif; ?>
            <li class="dropdown">					
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-copy"></i>
                    รายงาน
                    <b class="caret"></b>
                </a>	

                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url();?>report/">รายงานการลา</a></li>       
                </ul>    				
            </li>
            <?php if ($login_type == '0' || $login_type == '1' || $login_type == '2'): ?>
                <li class="dropdown">					
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-external-link"></i>
                        Account
                        <b class="caret"></b>
                    </a>	

                    <ul class="dropdown-menu">							
                        <li><a href="<?php echo base_url(); ?>profile">แก้ไขข้อมูลส่วนตัว</a></li>

                        <li><?php echo anchor('member/changePass', 'เปลี่ยนรหัสผ่าน'); ?></li>
                        <li><a href="#myModalAboute" data-toggle="modal">Aboute iLeave</a></li>

                    </ul>    				
                </li>
            <?php endif; ?>
            <?php if ($login_type == '3' || $login_type == '1' || $login_type == '2'): ?>
                <li class="dropdown">					
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-cogs"></i>
                        Systems.
                        <b class="caret"></b>
                    </a>	

                    <ul class="dropdown-menu">		
                        
                        <li><a href="<?php echo base_url();?>sysconfig">ตั้งค่าการส่งอีเมล์</a></li>
                      
                        <li><a href="#myModalAboute" data-toggle="modal">Aboute iLeave</a></li>
                    </ul>    				
                </li>


            </ul>
        <?php endif; ?>



    </div> <!-- /.nav-collapse -->

</div> <!-- /.container -->
<div class="modal fade hide" id="myModalAboute">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Aboute iLeave</h3>
    </div>
    <div class="modal-body">
        <p>เวอร์ชั่นล่าสุด  <?php $this->view('version_v'); ?></p>

        <p>ผู้พัฒนา : อีสานเดฟ</p>
        <p>เว็บไซต์ผู้พัฒนา : <a href ="http://www.esandev.com" target="_blank">www.esandev.com</a></p>
        <ul class="alert alert-error">
            <li>สงวนลิขสิทธิ์การนำไปใช้ ต้องได้รับอนุญาติหรือซื้อโปรแกรมจากผู้พัฒนาเท่านั้น จึงจะสามารถใช้โปรแกรมนี้ได้</li>
            <li>สอบถามข้อมูลหรือต้องการปรับปรุงแก้ไขโปรแกรมให้ติดต่อได้ที่ leksoft@hotmail.com </li>
            <li>เมื่อซื้อโปรแกรมแล้วสามารถนำไปพัฒนาเพิ่มเติมได้ทันทีโดยไม่ผิด</li>
        </ul>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Close</a>

    </div>
</div>