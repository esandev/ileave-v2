<script type="text/javascript">
    var chart1
    $(function () {
        chart1 = new Highcharts.Chart({
            chart: {
                renderTo: 'container_chart',
                type: 'bar'
            },
            title: {
                text: "<?php echo $title_text; ?>"
            },
            xAxis: {
                categories: <?php echo $categories_data; ?>
            },
            yAxis: {
                title: {
                    text: 'จำนวนการลา/ครั้ง'
                }
            },
            series: <?php echo $series_data; ?>
        });
    });
</script>


<div id="container_chart" style="min-width: 400px; height: 350px; margin: 0 auto"></div>