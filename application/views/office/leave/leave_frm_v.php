<link href="<?php echo base_url(); ?>assets/js/plugins/smartwizard/smart_wizard.modified.css" rel="stylesheet"> 
<script src="<?php echo base_url(); ?>assets/js/plugins/smartwizard/jquery.smartWizard-2.0.modified.js"></script>
<!-- datetimepicker -->

<link href="<?php echo base_url(); ?>assets/datetimepicker/jquery.datetimepicker.css" rel="stylesheet"> 
<script src="<?php echo base_url(); ?>assets/datetimepicker/jquery.datetimepicker.js"></script>

<!-- end -->
<script>
    $(function() {
        $('#alert_frm').css('display', 'none');
        $("#myform").validate({
            onkeyup: false,
        });
        $.metadata.setType("attr", "validate");

        $('#datefrom').mask("99/99/9999");
        $('#dateto').mask("99/99/9999");
        $("#datefrom").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#dateto").datepicker({ dateFormat: 'dd/mm/yy' });

    });
    function save() {

        if (($('#dateto').val()) <= ($('#datefrom').val())) {
            $.msgGrowl({
                type: 'warning'
                , title: 'ไม่สามารถบันทึกการลาได้'
                , text: 'ช่อง ถึงวันที่ ต้องมีค่ามากกว่า ช่อง ตั้งแต่วันที่'
                , position: 'bottom-center'

            });
        } else {
            $('#myform').submit();
        }
        return false;
    }

</script>
<!-- datetimepicker-->
<style type="text/css">  
    #startDate,  
    #endDate{  
        text-align:center;  
        width:100px;  
    }  
    #startTime,  
    #endTime{  
        text-align:center;  
        width:70px;  
    }        
  </style>      
  <script type="text/javascript">
$(function(){
    

    var optsDate = {  
        format:'Y-m-d', // รูปแบบวันที่ 
        formatDate:'Y-m-d',
        timepicker:false,   
        closeOnDateSelect:true,
    } 
    var optsTime = {
        format:'H:i', // รูปแบบเวลา
        step:30,  // step เวลาของนาที แสดงค่าทุก 30 นาที 
        formatTime:'H:i',
        datepicker:false,
    }    
    var setDateFunc = function(ct,obj){
        var minDateSet = $("#startDate").val();
        var maxDateSet = $("#endDate").val();
        
        if($(obj).attr("id")=="startDate"){
            this.setOptions({
                minDate:false,
                maxDate:maxDateSet?maxDateSet:false
            })                   
        }
        if($(obj).attr("id")=="endDate"){
            this.setOptions({
                maxDate:false,
                minDate:minDateSet?minDateSet:false
            })                   
        }
    }
    
    var setTimeFunc = function(ct,obj){
        var minDateSet = $("#startDate").val();
        var maxDateSet = $("#endDate").val();        
        var minTimeSet = $("#startTime").val();
        var maxTimeSet = $("#endTime").val();
        
        if(minDateSet!=maxDateSet){
            minTimeSet = false;
            maxTime = false;
        }
        
        if($(obj).attr("id")=="startTime"){
            this.setOptions({
                maxDate:maxDateSet?maxDateSet:false,
                minTime:false,
                maxTime:maxTimeSet?maxTimeSet:false        
            })                   
        }
        if($(obj).attr("id")=="endTime"){
            this.setOptions({
                minDate:minDateSet?minDateSet:false,
                maxTime:false,
                minTime:minTimeSet?minTimeSet:false      
            })                   
        }
    }    
    
    $("#startDate,#endDate").datetimepicker($.extend(optsDate,{  
        onShow:setDateFunc,
        onSelectDate:setDateFunc,
    }));
    
    $("#startTime,#endTime").datetimepicker($.extend(optsTime,{  
        onShow:setTimeFunc,
        onSelectTime:setTimeFunc,
    }));    
    
    
    
});
</script>       
 
<div class="container">
    <div class="row">

        <div class="span12">

            <div class="widget">

                <div class="widget-header">
                    <h3>
                        <i class="icon-magic"></i>
                        <?php echo $page_title; ?>							
                    </h3>
                    <div class="widget-actions">
                        <div class="btn-group">
                            <a class="btn dropdown-toggle btn-large btn-info" data-toggle="dropdown" href="javascript:;">
                                <i class="icon-file"></i>เปลี่ยนเรื่องลา <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <?php
                                $leavetype = $this->db->get('tbleave_type')->result_array();
                                foreach ($leavetype as $rs):
                                    ?>
                                    <li><a href="<?php echo base_url(); ?>formleave/index/<?php echo $rs['url']; ?>"><?php echo $rs['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div> <!-- /.widget-actions -->
                </div> <!-- /widget-header -->

                <div class="widget-content">
                    <?php echo form_open('formleave/save', array('id' => 'myform', 'class' => 'form-horizontal')); ?>
                    <input type="hidden" name="leave_type_id" value="<?php echo @$row_leave_type['id']; ?>"/>

                    <div id="" class="">
                        <div>	  				
                            <div class="row-fluid">

                                <div class="span6">

                                    
                                      <div class="control-group">
                                        <label class="control-label" for="title">เขียนที่</label>
                                        <div class="controls">
                                            <input type="text"  class="input-large" id="writing" name ="writing" value="">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="title">เรื่อง</label>
                                        <div class="controls">
                                            <input type="text"  class="input-large" id="title" name ="title" value="ขอ<?php echo @$row_leave_type['name']; ?>">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="president">เรียน <span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" class="input-large required" id="president" name ="president">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="member_name">ข้าพเจ้า</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" disabled="" id="member_name" value="<?php echo @$row_member['name']; ?>">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="position">ตำแหน่ง</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" disabled=""id="position" value="<?php echo @$row_member['position']; ?>">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="position">สังกัดหน่วยงาน</label>
                                        <div class="controls">
                                            <?php
                                            $depart = $this->db->get_where('tbdepart', array('id' => $row_member['depart_id']))->result_array();
                                            foreach ($depart as $rs):
                                                ?>
                                                <input type="text" class="input-large" disabled=""id="position" value="<?php echo @$rs['name']; ?>">
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="president">ขอลาเนื่องจาก</label>
                                        <div class="controls">
                                            <textarea cols="3" rows="3" style="width: 250px;" id="comment" name ="comment"></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="president">ตั้งแต่วันที่<span class="required">*</span></label>
                                        <div class="controls">
                                            <input class="input-medium" id="startDate" type="text" name ="datefrom" value="">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="president">ถึงวันที่<span class="required">*</span></label>
                                        <div class="controls">
                                            <input class="input-medium" id="endDate" type="text" name ="dateto" value="">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="president">ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่<span class="required">*</span></label>
                                        <div class="controls">
                                            <textarea cols="3" rows="3" style="width: 250px;" id="address" class="txt required" name ="address"></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="president">โทร</label>
                                        <div class="controls">
                                            <input type="text" disabled=""class="input-large" value="<?php echo @$row_member['mobile']; ?>">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="president"></label>
                                        <div class="controls">
                                            <button  onclick="return save();" class="btn btn-primary btn-large">บันทึกการลา</button>
                                        </div>
                                    </div>
                                    <div class="alert alert-info">
                                        <a class="close" data-dismiss="alert" href="#"></a>
                                        <h4 class="alert-heading">คำเตือน!</h4>
                                        เมื่อทำรายการเสร็จแล้ว ท่านจะไม่สามารถแก้ไขรายการได้อีก นอกจากยกเลิกรายการก่อน โปรดตรวจสอบข้อมูลว่าถูกต้องแล้ว ก่อนกดปุ่ม บันทึกการลา
                                    </div> <!-- ./alert -->
                                </div> <!-- /span6 -->

                                <div class="span5 offset1">

                                    <div class="well">
                                        <h3>แนวปฎิบัติและระเบียบการลาที่ควรทราบสำหรับผู้ลา</h3>
                                        <hr/>
                                        <p><?php echo @$row_leave_type['description']; ?></p>

                                    </div>

                                   
                                </div> <!-- /span6 -->
                            </div> <!-- /row-fluid -->


                        </div> <!-- /step -->
                    </div> <!-- /wizard -->
                    <?php echo form_close(); ?>
                </div> <!-- /widget-content -->
            </div> <!-- /widget -->
        </div> <!-- /.span12 -->
    </div> <!-- /row -->
</div> <!-- /.container -->
