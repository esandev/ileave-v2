<?php $this->view('ajax_loading_v'); ?>
<script type="text/javascript">
    $(function() {
        approve_load_div_grid();
        wait_load_div_grid();
        disapproval_load_div_grid();

        $("div.pagination a").live("click", function(event) {
            var p = {};
            p['txtsearch'] = $('#txtsearch').val();

            event.preventDefault();
            var url = $(this).attr("href");
            if (url != undefined) {
                $('#approve_div_grid').load(url, p);
                $('#wait_div_grid').load(url, p);
                $('#disapproval_div_grid').load(url, p);
            }
            return false;
        });


        $('#txtsearch').focus();
        $('#txtsearch').keydown(function(e) {
            if ((e.keyCode == 13)) {
                approve_load_div_grid();
                wait_load_div_grid();
                disapproval_div_grid();
                return false;
            }
        });

        $("#myform").validate({
            submitHandler: function() {
                save();
            }
        });
    });

    function approve_load_div_grid() {
        var p = {};
        p['txtsearch'] = $('#txtsearch').val();
        $('#approve_div_grid').load("<?php echo site_url('main/approve_ajax_get_grid') ?>", p);
        return false;
    }

    function wait_load_div_grid() {
        var p = {};
        p['txtsearch'] = $('#txtsearch').val();
        $('#wait_div_grid').load("<?php echo site_url('main/wait_ajax_get_grid') ?>", p);
        return false;
    }
    function disapproval_load_div_grid() {
        var p = {};
        p['txtsearch'] = $('#txtsearch').val();
        $('#disapproval_div_grid').load("<?php echo site_url('main/disapproval_ajax_get_grid') ?>", p);
        return false;
    }
    function save() {
        var p = {};
        p['id'] = $('#id').val();
        p['status'] = $('#status').val();
        p['title'] = $('#title').val();

        $.ajax({
            data: p,
            url: "<?php echo site_url('main/ajax_save') ?>",
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (data.msg == '1') {
                    //$.facebox(data.msg_text);
                    $('#alert_frm').text(data.msg_text).show();
                } else {
                    $('#myModal').modal('hide');
                    approve_load_div_grid();

                }
            },
            error: function() {
                alert('ไม่สามารถทำรายการได้ !!');
            }
        });
        return false;
    }

    function wait_grid_btn_edit(id) {

        var p = {};
        p['id'] = id;
        $.ajax({
            data: p,
            url: "<?php echo site_url('main/ajax_get_data') ?>",
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                $('#id').val(data.id);
                $('#writing').val(data.writing);
                $('#title').val(data.title);
                $('#president').val(data.president);
                $('#member_id').val(data.member_id);
                $('#leave_type_id').val(data.leave_type_id);
                $('#datefrom').val(data.datefrom);
                $('#dateto').val(data.dateto);
                $('#amountdate').val(data.amountdate);
                $('#comment').val(data.comment);
                $('#address').val(data.address);
                $('#status').val(data.status);
                $('#dateregist').val(data.dateregist);
                $('#newbihelp').val(data.newbihelp);
                $('#date_newbihelp').val(data.date_newbihelp);
                $('#ip').val(data.ip);

                $('#myModal').modal('show');
            },
            error: function() {
                alert('ไม่สามารถทำรายการได้ !!');
            }
        });
        return false;
    }

    function grid_btn_del(id) {
        if (confirm('คุณต้องการยกเลิกรายการนี้ ใช่หรือไม่?')) {
            var p = {};
            p['id'] = id;
            $.ajax({
                data: p,
                url: "<?php echo site_url('main/ajax_delete') ?>",
                type: 'POST',
                dataType: 'json',
                success: function() {
                    approve_load_div_grid();
                },
                error: function() {
                    alert('ไม่สามารถทำรายการได้ !!');
                }
            });
        }
        return false;
    }

    function clear_form() {
        $('label.error,#alert_frm').css('display', 'none');
        $('input').removeClass('error');

        $('#id').val('');
        $('#code').val('');
        $('#name').val('');
        return false;
    }
</script>
<div class="widget-header">
    <h3>
        <i class="icon-sitemap"></i> 
        แสดงการทำรายการ

    </h3>		

    <div class="widget-actions">
        <div class="btn-group">
            <a class="btn dropdown-toggle btn-large btn-info" data-toggle="dropdown" href="javascript:;">
                <i class="icon-file"></i>ยื่นเรื่องขอลา <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <?php
                $leavetype = $this->db->get('tbleave_type')->result_array();
                foreach ($leavetype as $rs):
                    ?>
                    <li><a href="<?php echo base_url(); ?>formleave/index/<?php echo $rs['url']; ?>"><?php echo $rs['name']; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div> <!-- /.widget-actions -->
</div> <!-- /.widget-header -->

<div class="widget-tabs">

    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#approve"><span class="label label-success"><?php echo $totalApprove; ?></span>&nbsp;&nbsp;&nbsp;การลาอนุมัติ</a>
        </li>
        <li><a href="#wait"><span class="label label-warning"><?php echo $totalwait; ?></span>&nbsp;&nbsp;&nbsp;การลารออนุมัติ</a></li>
        <li><a href="#disapproval"><span class="label label-important"><?php echo $totaldisapproval; ?></span>&nbsp;&nbsp;&nbsp;การลาไม่อนุมัติ</a></li>
    </ul>

</div> <!-- /.widget-tabs -->

<div class="widget-content">

    <div class="tab-content">
        <div class="tab-pane active" id="approve">

            <div id ="approve_div_grid"></div>

        </div>
        <div class="tab-pane" id="wait">
            <div id ="wait_div_grid"></div>
        </div>
        <div class="tab-pane" id="disapproval">
            <div id ="disapproval_div_grid"></div>
        </div>
    </div>

</div> <!-- /.widget-content -->


<!-- หน้าจอเพิ่มข้อมูล -->
<div class="modal hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form method="post" class="form-horizontal" id="myform">
        <div class="modal-header" style="padding:10px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4><?php echo $page_title; ?></h4>
        </div>
        <div class="modal-body" style="padding:10px;">
            <div class="alert alert-error" style="display:none" id="alert_frm">
            </div>
            <input type="hidden" id="id"/>
            <input type="hidden" id="leave_type_id" name ="leave_type_id"/>

            <div class="control-group">
                <label class="control-label" for="title">เปลี่ยนสถานะ</label>
                <div class="controls">
                    <select id ="status" name="status">
                        <option value="wait" selected>รออนุมัติ</option>
                        <option value="approve" >อนุมัติ</option>
                        <option value="disapproval" >ไม่อนุมัติ</option>
                        <option value="cancel" >ยกเลิก</option>

                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="title">เขียนที่</label>
                <div class="controls">

                    <input type="text"  class="input-large" disabled="" id="writing" name ="writing">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="title">เรื่อง</label>
                <div class="controls">
                    <?php
                    $row_leave_type = $this->db->get('tbleave_type')->row_array();
                    ?>
                    <input type="text"  class="input-large" disabled="" id="title" name ="title">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="president">เรียน </label>
                <div class="controls">
                    <input type="text" class="input-large required" disabled=""  id="president" name ="president">
                </div>
            </div>

            <!--
                            <div class="control-group">
                                <label class="control-label" for="">ชื่อภริยาโดยชอบตามกฎหมาย </label>
                                <div class="controls">
                                    <input type="text" class="input-large required" disabled=""  id="newbihelp" name ="newbihelp" value="">
                                </div>
                            </div>
            
                            <div class="control-group">
                                <label class="control-label" for="president">ซึ่งคลอดบุตรเมื่อวันที่ </label>
                                <div class="controls">
                                    <input type="text" class="input-large required" disabled=""  id="date_newbihelp" name ="date_newbihelp">
                                </div>
                            </div>
            -->
            <div class="control-group">
                <label class="control-label" for="member_name">ข้าพเจ้า</label>
                <div class="controls">
                    <?php
                    $row_member = $this->db->get('tbmember')->row_array();
                    ?>
                    <input type="text" class="input-large" disabled="" id="member_name" value="<?php echo @$row_member['name']; ?>">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="position">ตำแหน่ง</label>
                <div class="controls">
                    <input type="text" class="input-large" disabled=""id="position" value="<?php echo @$row_member['position']; ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="position">สังกัดหน่วยงาน</label>
                <div class="controls">
                    <?php
                    $depart = $this->db->get_where('tbdepart', array('id' => $row_member['depart_id']))->result_array();
                    foreach ($depart as $rs):
                        ?>
                        <input type="text" class="input-large" disabled=""id="position" value="<?php echo @$rs['name']; ?>">
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="president">ขอลาเนื่องจาก</label>
                <div class="controls">
                    <textarea cols="3" rows="3" style="width: 250px;" disabled="" id="comment" name ="comment"></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="president">ตั้งแต่วันที่</label>
                <div class="controls">
                    <input class="input-medium" id="datefrom" disabled="" type="text" name ="datefrom" value="">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="president">ถึงวันที่</label>
                <div class="controls">
                    <input class="input-medium" disabled=""  id="dateto" type="text" name ="dateto" value="">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="president">ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่</label>
                <div class="controls">
                    <textarea cols="3" rows="3"  disabled="" style="width: 250px;" id="address" class="txt required" name ="address"></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="president">โทร</label>
                <div class="controls">
                    <input type="text" disabled=""class="input-large" value="<?php echo @$row_member['mobile']; ?>">
                </div>
            </div>



        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true"> ปิด </button>
            <button class="btn btn-info" type="submit"><i class="icon-ok icon-white"></i> ยืนยันรายการ</button>
        </div>
    </form>
</div>
