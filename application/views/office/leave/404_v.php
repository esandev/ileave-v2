<div class="container">

    <div class="row">

        <div class="span12">

            <div class="error-container">
                <h1>Oops!</h1>

                <h2>404 Not Found</h2>

                <div class="error-details">
                    Sorry, an error has occured, Requested page not found!
                    <hr/>
                    ไม่พบหน้า <?php echo $page_title;?> อาจเกิดจากไม่มีฟอร์มที่รองรับการใช้งานนี้ กรุณาติดต่อผู้พัฒนาระบบ หากต้องการใช้แบบฟอร์มนี้
                </div> <!-- /error-details -->

                <div class="error-actions">
                    <a href="<?php echo base_url();?>main" class="btn btn-large btn-primary">
                        <i class="icon-chevron-left"></i>
                        &nbsp;
                        Back to Dashboard						
                    </a>

                    <a href="#myModalAboute" data-toggle="modal" class="btn btn-large">
                        <i class="icon-envelope"></i>
                        &nbsp;
                        Contact Support						
                    </a>

                </div> <!-- /error-actions -->

            </div> <!-- /error-container -->			

        </div> <!-- /span12 -->

    </div> <!-- /row -->

</div> <!-- /container -->