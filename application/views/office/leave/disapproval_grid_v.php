<script>
    $(function() {

        Slate.start();

    });
</script>
<div class="widget widget-table" >

    <div class="widget-content">

        <table class="table table-bordered table-striped " id="">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>ผู้ลา</th>
                    <th>วันที่ลา</th>
                    <th>สถานะ</th>
                    <th>วันที่ทำรายการ</th>
              
                </tr>
            </thead>
            <tbody>
                <?php
                $irow = 0;
                $i = $this->uri->segment(3) + 1;
                foreach ($query as $r) {
                    $irow++;
                    $id = $r['id'];
                    echo "<tr>";
                    echo "<td class='center'>" . $i++ . "</td>";
                    
                    $member = $this->db->get_where('tbmember', array('id' => $r['member_id']))->result_array();
                    foreach ($member as $mem) {
                        $leave = $this->db->get_where('tbleave_type', array('id' => $r['leave_type_id']))->result_array();
                        foreach ($leave as $le) {
                            echo "<td class='center '><a href ='javascript:;' class ='ui-popover' title ='ผู้ลา ". $mem['name'] ."' data-placement='left'  data-content='เรื่อง ขอ".$le['name']." เป็นเวลา ". $r['amountdate'] ." วัน'>" . $mem['name'] . "</a></td>";
                        }
                    }
                    
                    echo '<td align ="center">' . $this->mydate->dateToText($r['datefrom']) . ' - ' . $this->mydate->dateToText($r['dateto']) . '</td>';
           
                    echo '<td align="center"><span class="label label-important">' . $r['status'] . '</span></td>';
                    echo '<td align="center">' . $this->mydate->dateToText($r['dateregist']) . '</td>';
                   
                    echo "</tr>";
                }
                if ($irow == 0) {
                    echo "<tr><td colspan='3' class='center'>*** ไม่พบข้อมูล ***</td></tr>";
                }
                ?>
            </tbody>
        </table>		

    </div> <!-- /.widget-content -->

    <?php echo $pagination; ?>
</div> <!-- /.widget -->