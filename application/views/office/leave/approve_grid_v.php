<script>
    $(function() {

        Slate.start();

    });
</script>

<div class="widget widget-table" >

    <div class="widget-content">

        <table class="table table-bordered table-striped " id="">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>ผู้ลา</th>
                    <th>วันที่ลา</th>
                    <th>สถานะ</th>
                    <th>วันที่ทำรายการ</th>
                    <th width="90">Menu Option</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $irow = 0;
                $i = $this->uri->segment(3) + 1;
                foreach ($query as $r) {
                    $irow++;
                    $id = $r['id'];
                    echo "<tr>";
                    echo "<td class='center'>" . $i++ . "</td>";

                    $member = $this->db->get_where('tbmember', array('id' => $r['member_id']))->result_array();
                    foreach ($member as $mem) {
                        $leave = $this->db->get_where('tbleave_type', array('id' => $r['leave_type_id']))->result_array();
                        foreach ($leave as $le) {
                            echo "<td class='center '><a href ='javascript:;' class ='ui-popover' title ='ผู้ลา " . $mem['name'] . "' data-placement='left'  data-content='เรื่อง ขอ" . $le['name'] . " เป็นเวลา " . $r['amountdate'] . " วัน'>" . $mem['name'] . "</a></td>";
                        }
                    }

                    echo '<td align ="center">' . $this->mydate->dateToText($r['datefrom']) . ' - ' . $this->mydate->dateToText($r['dateto']) . '</td>';
                    echo '<td align="center"><span class="label label-success">' . $r['status'] . '</span></td>';
                    echo '<td align="center">' . $this->mydate->dateToText($r['dateregist']) . '</td>';

                    //ตรวจสอบการ login
                    $s_login = $this->session->userdata('s_login');
                    $member_id = $s_login['login_id'];
                    $mem_type = $s_login['login_type'];
                    if ($mem_type == '3' || $mem_type == '1' || $mem_type == '2'):
                        echo "<td class='center'>";
                        echo '<a href="#" onclick="return wait_grid_btn_edit(\'' . $id . '\')" class="btn btn-mini"><i class="icon-pencil"></i>เปลี่ยนสถานะ</a>&nbsp;';

                        echo "</td>";
                    else:
                        echo "<td class='center'>";
                        switch ($r['leave_type_id']) {
                            case '1':
                                echo '<a href="' . base_url() . 'formleave/printf/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '2':
                                echo '<a href="' . base_url() . 'formleave/printf/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '3':
                                echo '<a href="' . base_url() . 'formleave/printf/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '4':
                                echo '<a href="' . base_url() . 'formleave/printf_rest/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '5':
                                echo '<a href="' . base_url() . 'formleave/printf_help_his_wife/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '6':
                                echo '<a href="' . base_url() . '#/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '7':
                                echo '<a href="' . base_url() . '#/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '8':
                                echo '<a href="' . base_url() . '#/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '9':
                                echo '<a href="' . base_url() . '#/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '10':
                                echo '<a href="' . base_url() . '#/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            case '11':
                                echo '<a href="' . base_url() . '#/' . $id . '" class="btn btn-mini"><i class="icon-print"></i>พิมพ์ใบลา</a>&nbsp;';
                                break;
                            default :
                                echo "ไม่พบแบบฟอร์มนี้";
                        }
                        echo "</td>";

                    endif;
                    echo "</tr>";
                }
                if ($irow == 0) {
                    echo "<tr><td colspan='3' class='center'>*** ไม่พบข้อมูล ***</td></tr>";
                }
                ?>
            </tbody>
        </table>		

    </div> <!-- /.widget-content -->

    <?php echo $pagination; ?>
</div> <!-- /.widget -->
<span class="label label-warning">หมายเหตุ</span><u> พิมพ์ได้เฉพาะรายการลาที่ถูกอนุมัติแล้วเท่านั้น</u>

