<?php $this->view('ajax_loading_v'); ?>

<?php if (isset($msg_text)): ?>
    <script type="text/javascript">
        $(function(){
            var msg_text = "<?php echo $msg_text; ?>";
            $.facebox(msg_text);
        });
    </script>
<?php endif; ?>

<script type="text/javascript">
    $(function(){
        loaddepart();
    });
    
    function loaddepart(){
        var p = {};
        p['member_id'] = $('#member_id').val();
        $('#div_todepart').load("<?php echo site_url('membertodepart/ajax_todepart') ?>",p);
        return false;
    }

</script>
<div class="textbox">
    <div class="toolbar">
        <div class="pull-left title"><?php echo $page_title; ?></div>
        <div class="pull-right">
            <?php echo anchor('membertodepart', "<i class='icon-remove-circle'></i> ปิด", array('class' => 'btn')); ?>
        </div>
        <div class="clear"></div>
    </div>
    <div class="textbox_content">
        <b>ผู้อนุมัติ :</b> <?php echo $row['name']; ?>
        <input type="hidden" id="member_id" name="member_id" value="<?php echo $row['id']; ?>"/>
        <div id="div_todepart"></div>
    </div>
</div>
