<script type="text/javascript">
    $(function(){
        $('#chk_todepart_all').click(
        function(){
            $("#todepart_table INPUT[type='checkbox']").attr('checked', $('#chk_todepart_all').is(':checked'));
        });

        $('#chk_depart_all').click(
        function(){
            $("#depart_table INPUT[type='checkbox']").attr('checked', $('#chk_depart_all').is(':checked'));
        });
    });
    
    function depart_check(){
        var data_array=new Array();
        var i=0;
        $("#depart_table tbody tr td:first-child input:checkbox").each(function(){
            if(this.checked==true){
                data_array[i]=this.name;
                i++;
            }
        });

        if(data_array.length>0){
            var p = {};
            p['member_id'] = $('#member_id').val();
            p['data_array'] = data_array;
            $.ajax({
                data:p,
                url:"<?php echo site_url('membertodepart/ajax_save') ?>",
                type:'POST',
                dataType:'json',
                success: function(){
                    loaddepart();
                },
                error:function(){
                    alert('ไม่สามารถทำรายการได้ !!');
                }
            });
            return false;
        }
    }

    function depart_cancel(){
        var data_array=new Array();
        var i=0;
        $("#todepart_table tbody tr td:first-child input:checkbox").each(function(){
            if(this.checked==true){
                data_array[i]=this.name;
                i++;
            }
        });

        if(data_array.length>0){
            var p = {};
            p['data_array'] = data_array;
            $.ajax({
                data:p,
                url:"<?php echo site_url('membertodepart/ajax_cancel') ?>",
                type:'POST',
                dataType:'json',
                success: function(){
                    loaddepart();
                },
                error:function(){
                    alert('ไม่สามารถทำรายการได้ !!');
                }
            });
        }
        return false;
    }
</script>

<table border="0" cellspacing="00" width="100%">
    <tr valign="top">
        <td>
            <div class="table-search" style="height:20px">
                <strong>แผนกที่มีสิทธิ์ อนุมัติการจอง</strong>
            </div>
            <table class="display normal-t" cellspacing="0" cellpadding="0" border="0" id="todepart_table">
                <thead>
                    <tr>
                        <th width="40"><input type="checkbox" id="chk_todepart_all"/></th>
                        <th align="left">ชื่อแผนก</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $irow = 0;
                    foreach ($todepart as $r) {
                        $irow++;
                        echo '<tr>';
                        echo "<td align=\"center\"><input type=\"checkbox\" name='" . $r['id'] . "'></td>";
                        echo '<td>' . $r['name'] . '</td>';
                        echo '</tr>';
                    }
                    if ($irow == 0) {
                        echo '<td colspan="2">***ไม่มีข้อมูล***</td>';
                    }
                    ?>
                </tbody>
            </table>
            <div style="padding-top: 10px">
                <a href="#" class="btn btn-danger" onclick="return depart_cancel();"><i class="icon-chevron-right icon-white"></i> ยกเลิกสิทธิ์อนุมัติ</a>
            </div>
        </td>
        <td width="50%" style="padding-left:10px">
            <div class="table-search" style="height:20px">
                <strong>แผนกที่ไม่มีสิทธิ์ อนุมัติการจอง</strong>
            </div>
            <table class="display normal-t" cellspacing="0" cellpadding="0" border="0" id="depart_table">
                <thead>
                    <tr>
                        <th width="40"><input type="checkbox" id="chk_depart_all"/></th>
                        <th align="left">ชื่อแผนก</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $irow = 0;
                    foreach ($depart as $r) {
                        $irow++;
                        echo '<tr>';
                        echo "<td align=\"center\"><input type=\"checkbox\" name='" . $r['id'] . "'></td>";
                        echo '<td>' . $r['name'] . '</td>';
                        echo '</tr>';
                    }
                    if ($irow == 0) {
                        echo '<td colspan="2">***ไม่มีข้อมูล***</td>';
                    }
                    ?>
                </tbody>
            </table>
            <div style="padding-top: 10px">
                <a href="#" class="btn btn-primary" onclick="return depart_check();"><i class="icon-chevron-left icon-white"></i> กำหนดสิทธิ์อนุมัติ</a>
            </div>
        </td>
    </tr>
</table>
