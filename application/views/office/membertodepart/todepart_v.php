<?php $this->view('ajax_loading_v'); ?>
<script type="text/javascript">
    $(function(){
        load_div_grid();

        $("div.pagination a").live("click",function(event){
            var p = {};
            p['txtsearch'] = $('#txtsearch').val();

            event.preventDefault();
            var url=$(this).attr("href");
            if(url!=undefined){
                $('#div_grid').load(url,p);
            }
            return false;
        });


        $('#txtsearch').focus();
        $('#txtsearch').keydown(function(e){
            if((e.keyCode==13)){
                load_div_grid();
                return false;
            }
        });
        
        $("#myform").validate({
            submitHandler: function() {
                save();
            }
        });
    });

    function load_div_grid(){
        var p = {};
        p['txtsearch'] = $('#txtsearch').val();
        $('#div_grid').load("<?php echo site_url('membertodepart/ajax_get_grid') ?>",p);
        return false;
    }
</script>
<div class="textbox">
    <div class="toolbar">
        <div class="pull-left title"><?php echo $page_title; ?></div>
        <div class="pull-right">
            <?php echo anchor('main', "<i class='icon-remove-circle'></i> ปิด", array('class' => 'btn'));?>
        </div>
        <div class="clear"></div>
    </div>
    <div class="textbox_content">
        <div class="table-search">
            <?php echo form_open('', array('class' => 'form-inline')); ?>
            <label><b>ค้นหา</b></label>
            <div class="input-append">
                <input class="span4" id="txtsearch" name="txtsearch" type="text">
                <button class="btn" type="button" onclick="return load_div_grid();"><i class="icon-search"></i> ค้นหา</button>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div id="div_grid"></div>
    </div>
</div>
