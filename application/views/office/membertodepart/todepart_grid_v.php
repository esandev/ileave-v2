<table class="display normal-t" cellspacing="0" cellpadding="0" border="0">
    <thead>
        <tr>
            <th width="200">ชื่อ-สกุล</th>
            <th width="150">ชื่อเข้าใช้งาน</th>
            <th>สังกัดแผนก</th>
            <th width="140">เมนู</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $irow = 0;
        foreach ($query as $r) {
            $irow++;
            $id=$r['id'];
            
            echo "<tr>";
            echo "<td>".$r['name']."</td>";
            echo "<td>".$r['username']."</td>";
            echo "<td>".$r['depart_name']."</td>";
            echo "<td class='center'>";
            echo anchor("membertodepart/edit/$id",'<i class="icon-pencil"></i>กำหนดแผนกที่ดูแล',array('class'=>'btn btn-mini')).  nbs();
            echo "</td>";
            echo "</tr>";
        }
        if($irow==0){
            echo "<tr><td colspan='4' class='center'>*** ไม่พบข้อมูล ***</td></tr>";
        }
        ?>
    </tbody>
</table>
<?php echo $pagination; ?>