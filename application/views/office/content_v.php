<?php
$this->view('ajax_loading_v');
$s_login = $this->session->userdata('s_login');
$member_id = $s_login['login_id'];
?>

<link href="<?php echo base_url(); ?>assets/js/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/pages/calendar.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>
<?php if (isset($msg_text)): ?>
    <script>
        $(function() {

            $.msgGrowl({
                type: '<?php echo $msg_type; ?>'
                , title: 'แจ้งเตือนการทำรายการ'
                , text: '<?php echo $msg_text; ?>'
                , position: 'top-center'

            });

        });
    </script>
<?php endif; ?>
<?php if (isset($msg_text_save)): ?>
    <script>
        $(function() {

            $.msgGrowl({
                type: '<?php echo $msg_type_save; ?>'
                , title: 'แจ้งเตือนการทำรายการ'
                , text: '<?php echo $msg_text_save; ?>'
                , position: 'top-center'

            });

        });
    </script>
<?php endif; ?>

<script>
    $(function() {

        $('#datepicker-inline').datepicker();
        $('#datepicker-inline2').datepicker();

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#calendar-holder').fullCalendar({
            header: {
                left: 'prev, next',
                center: 'title',
                right: 'month,basicWeek,basicDay,'
            },
            events: [
                {
                    title: 'Leksoft Fei ลาพักผ่อน',
                    start: new Date(y, m, d - 5),
                    end: new Date(y, m, d - 2),
                    color: Slate.chartColors[1]
                },
                {
                    title: 'นครินทร์ ลาป่วย',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false,
                    color: Slate.chartColors[1]
                },
                {
                    title: 'นครินทร์ ลากิจ ',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/',
                    color: Slate.chartColors[0]
                }
            ]
        });

    });
</script>

<div class="container">

    <div class="row">

        <div class="span4">

            <div class="widget">

                <div class="widget-header">
                    <h3>
                        <i class="icon-bookmark"></i> 
                        Shortcuts								
                    </h3>

                    <div class="widget-actions">
                        <span class="badge">8</span>	
                    </div> <!-- /.widget-actions -->

                </div> <!-- /.widget-header -->

                <div class="widget-content">

                    <div class="shortcuts">
                        

                       

                        <a href="<?php echo base_url();?>report" class="shortcut">
                            <i class="shortcut-icon icon-signal"></i>
                            <span class="shortcut-label">สถิติการลา</span>	
                        </a>

                        

                        <a href="<?php echo base_url(); ?>profile" class="shortcut">
                            <i class="shortcut-icon icon-user"></i>
                            <span class="shortcut-label">ข้อมูลส่วนตัว</span>
                        </a>

                        <a href="javascript:;" class="shortcut">
                            <i class="shortcut-icon icon-file"></i>
                            <span class="shortcut-label">Massages</span>	
                        </a>

                    </div> <!-- /.shortcuts -->

                </div> <!-- /.widget-content -->

            </div> <!-- /.widget -->
            <div class="widget">
                <div class="widget-header">

                    <h3>
                        <i class="icon-tasks"></i> 
                        งบประมาณ 2557								
                    </h3>

                </div> <!-- /.widget-header -->

                <div class="widget-content">		

                    <p>แสดงสถิติการลาในปีงบประมาณนี้.<span class="label label-important">แสดงเฉพาะรายการที่อนุมัติ</span></p>
                    <?php
                    $sql = "SELECT * ,COUNT(leave_type_id) AS total_leave FROM tbleavemanage WHERE member_id = '$member_id' AND status = 'approve' GROUP BY leave_type_id";
                    $leave = $this->db->query($sql)->result_array();
                    foreach ($leave as $rs):
                        ?>
                        <div class="stat">
                            <?php
                            $leavetype = $this->db->get_where('tbleave_type', array('id' => $rs['leave_type_id']))->result_array();
                            foreach ($leavetype as $r):
                                ?>
                                <div class="stat-header">


                                    <div class="stat-label">
                                        <?php echo $r['name']; ?>

                                    </div> <!-- /.stat-label -->

                                    <div class="stat-value">
                                        <?Php
                                        echo $rs['total_leave'] . " ครั้ง";
                                        ?>

                                    </div> <!-- /.stat-value -->

                                </div> <!-- /stat-header -->

                                <div class="progress progress-info progress-striped">
                                    <div class="bar" style="width: <?php echo $rs['total_leave']; ?>%;"></div> <!-- /.bar -->
                                </div> <!-- /.progress -->
                            <?php endforeach; ?>
                        </div> <!-- /.stat -->
                    <?php endforeach; ?>
                    <br />

                </div> <!-- /.widget-content -->

            </div> <!-- /.widget -->

        </div> <!-- /.span4 -->

        <div class="span8">

            <div class="widget">

                <?php $this->view('office/leave/show_list_v'); ?>

            </div> <!-- /.widget -->

           

        </div> <!-- /.span8 -->

    </div> <!-- /.row -->

</div> <!-- /.container -->
