
<div class="widget widget-table" >

    <div class="widget-content">

        <table class="table table-bordered table-striped " id="">
            <thead>
                <tr>
                    <th width="80">No.</th>
                    <th width="120">รหัสแผนก</th>
                    <th>ชื่อแผนก</th>
                    <th width="200">ชื่อหน่วยงาน</th>
                    <th width="120">Menu Option</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $irow = 0;
                $i = $this->uri->segment(3) + 1;
                foreach ($query as $r) {
                    $irow++;
                    $id = $r['id'];
                    echo "<tr>";
                    echo "<td class='center'>" . $i++ . "</td>";
                    echo "<td class='center'>" . $r['code'] . "</td>";
                    echo "<td>" . $r['name'] . "</td>";
                    echo "<td>" . $r['team_name'] . "</td>";
                    echo "<td class='center'>";
                    echo '<a href="#" onclick="return grid_btn_edit(\'' . $id . '\')" class="btn btn-mini"><i class="icon-pencil"></i>แก้ไข</a>&nbsp;';
                    echo '<a href="#" onclick="return grid_btn_del(\'' . $id . '\')" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i>ลบ</a>';
                    echo "</td>";
                    echo "</tr>";
                }
                if ($irow == 0) {
                    echo "<tr><td colspan='4' class='center'>*** ไม่พบข้อมูล ***</td></tr>";
                }
                ?>
            </tbody>
        </table>
        <?php echo $pagination; ?>
    </div>