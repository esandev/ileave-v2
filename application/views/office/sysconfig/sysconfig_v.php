<?php $this->view('ajax_loading_v'); ?>

<?php if (isset($msg_text)): ?>
    <script>
        $(function() {

            $.msgGrowl({
                type: 'success'
                , title: 'Message'
                , text: '<?php echo $msg_text; ?>'
                , position: 'top-center'
            });
        });
    </script>

<?php endif; ?>

<script type="text/javascript">
    $(function(){
        $('#alert_frm').css('display', 'none');     
    });

    function testMail(){
        $('#alert_frm').hide();
        var p = {};
        p['email'] = $('#email').val();
        p['email2'] = $('#email2').val();
        p['email_password'] = $('#email_password').val();

        $.ajax({
            data:p,
            url:"<?php echo site_url('sysconfig/ajax_test_sendmail') ?>",
            type:'POST',
            dataType:'json',
            success: function(data){
                $('#alert_frm').text(data.msg).show();
            },
            error:function(){
                alert('ไม่สามารถส่ง Mail ได้ !!');
            }
        });
    }
    
    function save(){
        $('#myform').submit();
        return false;
    }
</script>
<div class="container">
  <div class="widget-header">
            <h3>	      					
                <i class="icon-envelope"></i>
                <?php echo $page_title; ?>      					
            </h3>
       <div class="pull-right">
            <a href="#" class="btn" onclick="return save();"><i class="icon-hdd"></i> บันทึกการตั้งค่าระบบ</a>
            <?php echo anchor('main', "<i class='icon-remove-circle'></i> ปิด", array('class' => 'btn')); ?>
        </div>
        </div> <!-- /widget-header -->
       

 <div class="widget-content">
        <div class="alert alert-info" style="display:none" id="alert_frm"></div>
        <?php echo form_open('sysconfig/save', array('id' => 'myform', 'class' => 'form-horizontal')); ?>
        <input type="hidden" id="id" name="id" value="<?php echo @$row['id']; ?>"/>
        <input type="hidden" id="id" name="id" value="<?php echo @$row['id']; ?>"/>

        <div class="alert alert-info"><i class="icon-bullhorn"></i> หากเปิดใช้ระบบส่งเมลล์ เมื่อทำรายการลา, การอนุมัติการลา ระบบจะทำงานช้าลงเล็กน้อยเนื่องจากต้องทำการส่งเมลล์ด้วย</div>
        <table border="0" cellpadding="4" cellspacing="1" class="tblform">
            <tbody>
                <tr>
                    <td class="td1" width="150">เปิดใช้ระบบส่งเมลล์ :</td>
                    <td class="td2">
                        <?php $sendemail = (@$row['sendemail'] == '1') ? 'checked' : ''; ?>
                        <input type="checkbox" id="sendemail" name="sendemail" <?php echo $sendemail; ?>/>
                        เปิดใช้
                    </td>
                </tr>
                <tr>
                    <td class="td1" valign="top">อีเมลล์ของระบบ :</td>
                    <td class="td2">
                        <input type="text" class="txt" id="email" name="email" value="<?php echo @$row['email']; ?>" style="width: 300px"/><br/>
                    </td>
                </tr>
                <tr>
                    <td class="td1" valign="top">รหัสผ่านอีเมลล์ :</td>
                    <td class="td2">
                        <input type="text" class="txt" id="email_password" name="email_password" value="<?php echo @$row['email_password']; ?>" style="width: 300px"/>
                        <br/><div class="alert alert-info">
                            <ul>
                                <li><b>อีเมลล์ของระบบ</b> คืออีเมลล์หลักที่จะใช้ส่งให้กับผู้ใช้งานคนอื่นๆ <b>ซึ่งจะต้องเป็นอีเมลล์ของ Gmail เท่านั้น</b></li>
                                <li>ระบบเครือข่ายต้องอนุญาติให้ใช้งาน <b>Protocol SMTP</b></li>
                                <li>ระบบเครือข่ายต้องอนุญาติให้ใช้งาน <b>Port 465</b></li>
                            </ul>
                    </td>
                </tr>
                <tr>
                    <td class="td1" valign="top">อีเมลล์ทดสอบรับเข้า :</td>
                    <td class="td2">
                        <div class="input-append">
                            <input type="text" class="txt" id="email2" name="email2" value="<?php echo @$row['email2']; ?>" style="width: 300px"/>&nbsp;<a href="#" class="btn" onclick="testMail();"><i class='icon-globe'></i> ทดสอบส่ง Mail</a><br/>
                        </div>
                        <div class="alert alert-info">คืออีเมลล์ที่ใช้รับในกรณีที่ผู้ใช้ทำการลา <b>จะใช้อีเมลล์ของค่ายไหนก็ได้ที่มีอยู่จริง</b></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php echo form_close(); ?>

    </div>
</div>
