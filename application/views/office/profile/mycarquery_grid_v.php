<table class="display normal-t" cellspacing="0" cellpadding="0" border="0">
    <thead>
        <tr>
            <th width="100">วันที่จอง</th>
            <th>จองใช้เพื่อ</th>
            <th width="120">ทะเบียน</th>
            <th width="120">สถานะ</th>
            <th width="120">เมนู</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $irow = 0;
        foreach ($query as $r) {            
            $irow++;
            $id=$r['doc_id'];
            echo "<tr>";
            echo "<td class='center'>".$mydate->dateToText($r['rdate'])."</td>";
            echo "<td>".$r['name']."</td>";
            echo "<td class='center'>".$r['car_code']."</td>";
            echo "<td class='center'>".$r['status_text']."</td>";
            echo "<td class='center'>";
           echo anchor("mycarquery/show/$id", '<i class="icon-folder-open"></i> แสดง', array('class' => 'btn btn-mini'));
            echo "</td>";
            echo "</tr>";
        }
        if($irow==0){
            echo "<tr><td colspan='5' class='center'>*** ไม่พบข้อมูล ***</td></tr>";
        }
        ?>
    </tbody>
</table>
<?php echo $pagination; ?>