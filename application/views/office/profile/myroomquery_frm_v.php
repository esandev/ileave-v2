<?php $this->view('ajax_loading_v'); ?>

<?php if (isset($msg_text)): ?>
    <script type="text/javascript">
        $(function(){
            var msg_text = "<?php echo $msg_text; ?>";
            $.facebox(msg_text);
        });
    </script>
<?php endif; ?>

<script type="text/javascript">
    function cancelconfirm(){
        if(confirm('คุณต้องการยกเลิกรายการจอง รายการนี้ใช่หรือไม่ !!')){
            $('#myform').submit();
        }
        return false;
    }
</script>

<div class="textbox">
    <div class="toolbar">
        <div class="pull-left title"><?php echo $page_title; ?></div>
        <div class="pull-right">
            <?php echo anchor('myroomquery/preview/' . $row['id'], "<i class='icon-print'></i> พิมพ์ใบจอง", array('class' => 'btn', 'target' => '_blank')); ?>
            <?php echo anchor('myroomquery', "<i class='icon-remove-circle'></i> ปิด", array('class' => 'btn')); ?>
        </div>
        <div class="clear"></div>
    </div>
    <div class="textbox_content">
        <div id="div_message" class="alert alert-info" style="display:none">ระบบกำลังบันทึกข้อมูล และทำการส่งอีเมลล์ อาจจะต้องใช้เวลาสักครู....</div>
        <?php echo form_open('myroomquery/cancel', array('class' => 'form-horizontal', 'id' => 'myform')); ?>
        <input type="hidden" id="id" name="id" value="<?php echo $row['id']; ?>"/>
        <input type="hidden" name="member_id" value="<?php echo $row['member_id']; ?>"/>
        <table border="0" cellpadding="6" cellspacing="1" class="tblform-line">
            <tbody>
                <tr>
                    <td class="td1" width="150">วาระการประชุม :</td>
                    <td><?php echo $row['meet_name']; ?></td>
                </tr>
                <tr>
                    <td class="td1">ประธานการประชุม :</td>
                    <td><?php echo $row['chairman']; ?></td>
                </tr>
                <tr>
                    <td class="td1">จำนวนผู้เข้าประชุม :</td>
                    <td><?php echo $row['amount_people']; ?> คน</td>
                </tr>
                <tr>
                    <td class="td1" valign="top">รายชื่อผู้เข้าประชุม :</td>
                    <td><?php echo nl2br($row['employee']); ?></td>
                </tr>
                <tr>
                    <td class="td1" valign="top">ห้องประชุม :</td>
                    <td>
                        <?php echo $row['room_name'] . "&nbsp;&nbsp;<b>วันที่</b> " . $mydate->dateToText($row['rdate']); ?>&nbsp;&nbsp;<b>จากเวลา :</b><?php echo $row['start_time']; ?>&nbsp;-&nbsp;<?php echo $row['end_time']; ?>
                        <?php
                        foreach ($booking as $r) {
                            echo "<br/>" . $r['room_name'] . "&nbsp;&nbsp;<b>วันที่</b> " . $mydate->dateToText($r['rdate']) . "&nbsp;&nbsp;<b>จากเวลา :</b>" . $r['start_time'] . "&nbsp;-&nbsp;" . $r['end_time'];
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="td1" valign="top">อุปกรณ์ที่ต้องการ :</td>
                    <td>
                        <?php
                        foreach ($equip as $r) {
                            echo $r['name'] . ", ";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="td1" valign="top">อุปกรณ์เพิ่มเติม :</td>
                    <td><?php echo nl2br($row['equip']); ?></td>
                </tr>
                <tr>
                    <td class="td1" valign="top">หมายเหตุ :</td>
                    <td><?php echo nl2br($row['meet_detail']); ?></td>
                </tr>
                <tr>
                    <td class="td1">ผู้ติดต่อ :</td>
                    <td><?php echo $row['member_name']; ?></td>
                </tr>
                <tr>
                    <td class="td1">แผนก :</td>
                    <td><?php echo $row['depart_name']; ?></td>
                </tr>
                <tr>
                    <td class="td1">โทรศัพท์ :</td>
                    <td><?php echo $row['mobile'] . "," . $row['tel']; ?></td>
                </tr>
                <tr>
                    <td class="td1">เอกสารประกอบ :</td>
                    <td>
                        <?php
                        if ($row['file_name'] != '') {
                            echo "<a href='" . base_url() . "assets/upload/" . $row['file_tmpname'] . "'>" . $row['file_name'] . "</a>";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="td1">ผู้อนุมัติ :</td>
                    <td><?php echo @$approve['name'] . "&nbsp;&nbsp;&nbsp;<b>โทรศัพท์</b> " . @$approve['mobile'] . "," . @$approve['tel']; ?></td>
                </tr>
                <tr>
                    <td class="td1">วันที่อนุมัติ :</td>
                    <td>
                        <?php echo $row['approve_time']; ?>
                    </td>
                </tr>
                <tr>
                    <td class="td1">ผลการอนุมัติ :</td>
                    <td><?php echo $row['status_text']; ?></td>
                </tr>
                <tr>
                    <td class="td1">หมายเหตุการอนุมัติ :</td>
                    <td><?php echo $row['status_detail'];?></td>
                </tr>
                <?php if ($row['status']=='0'): ?>
                <tr>
                    <td></td>
                    <td style="padding-top:10px">
                        <?php echo anchor('#', "<i class='icon-trash icon-white'></i> ยกเลิกรายการจอง", array('class' => 'btn btn-danger', 'onclick' => 'return cancelconfirm();')); ?>
                    </td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <?php echo form_close(); ?>
    </div>
</div>
