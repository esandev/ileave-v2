<?php $this->view('ajax_loading_v'); ?>
<script type="text/javascript">
    $(function(){
        load_div_grid();

        $("div.pagination a").live("click",function(event){
            var p = {};
            p['start_date'] = $('#start_date').val();
            p['end_date'] = $('#end_date').val();
            p['status']= $('#status').val();
            p['fieldsearch'] = $('#fieldsearch').val();
            p['txtsearch'] = $('#txtsearch').val();

            event.preventDefault();
            var url=$(this).attr("href");
            if(url!=undefined){
                $('#div_grid').load(url,p);
            }
            return false;
        });


        $('#txtsearch').focus();
        $('#txtsearch').keydown(function(e){
            if((e.keyCode==13)){
                load_div_grid();
                return false;
            }
        });
        
        $("#start_date").mask("99/99/9999");
        $("#end_date").mask("99/99/9999");
    });

    function load_div_grid(){
        if($('#start_date').val()=='' || $('#end_date').val()==''){
            alert("คุณต้องกรอกวันที่ให้ครบ");
        }else{
            var p = {};
            p['start_date'] = $('#start_date').val();
            p['end_date'] = $('#end_date').val();
            p['status']= $('#status').val();
            p['fieldsearch'] = $('#fieldsearch').val();
            p['txtsearch'] = $('#txtsearch').val();
            $('#div_grid').load("<?php echo site_url('myroomquery/ajax_get_grid') ?>",p);
        }
        return false;
    }
</script>
<div class="textbox">
    <div class="toolbar">
        <div class="pull-left title"><?php echo $page_title; ?></div>
        <div class="pull-right">
            <?php echo anchor('main', "<i class='icon-remove-circle'></i> ปิด", array('class' => 'btn')); ?>
        </div>
        <div class="clear"></div>
    </div>
    <div class="textbox_content">
        <div class="table-search2">
            <?php echo form_open('', array('class' => 'form-inline', 'style' => 'margin-bottom:4px')); ?>
            <table border="0" width="100%">
                <tr>
                    <td>
                        จากวันที่จอง&nbsp;&nbsp;
                        <div class="input-append">
                            <input type='text' id='start_date' name='start_date' style="width:80px" value="<?php echo $start_date; ?>" class="required"/>
                            <span class="btn" type="button" id="cal_start_date" tabindex="-1"><i class="icon-calendar"></i></span>
                            <script type="text/javascript">
                                Calendar.setup({
                                    inputField    : "start_date",
                                    button        : "cal_start_date",
                                    ifFormat    : "%d/%m/%Y"
                                });
                            </script>
                        </div>&nbsp;&nbsp;
                        ถึง&nbsp;&nbsp;
                        <div class="input-append">
                            <input type='text' id='end_date' name='end_date' style="width:80px" value="<?php echo $end_date; ?>" class="required"/>
                            <span class="btn" type="button" id="cal_end_date" tabindex="-1"><i class="icon-calendar"></i></span>
                            <script type="text/javascript">
                                Calendar.setup({
                                    inputField    : "end_date",
                                    button        : "cal_end_date",
                                    ifFormat    : "%d/%m/%Y"
                                });
                            </script>
                        </div>
                        <?php echo nbs(5); ?>
                        สถานะ&nbsp;
                        <select id="status" name="status" style="width:160px">
                            <option value="99">***แสดงทุกสถานะ***</option>
                            <option value="0">รออนุมัติ</option>
                            <option value="1">อนุมัติ</option>
                            <option value="2">ไม่อนุมัติ</option>
                            <option value="3">ยกเลิกจอง</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="padding-top:5px">
                            ค้นตาม
                            <select id="fieldsearch" name="fieldsearch" style="width:160px">
                                <option value="tbreserv.meet_name">วาระการประชุม</option>
                                <option value="tbroom.name">ห้องประชุม</option>
                                <option value="tbdepart.name">แผนก</option>
                            </select>
                            คำค้น
                            <div class="input-append">
                                <input class="span4" id="txtsearch" name="txtsearch" type="text">
                                <button class="btn" type="button" onclick="return load_div_grid();"><i class="icon-search"></i> ค้นหา</button>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <?php echo form_close(); ?>
        </div>
        <div id="div_grid"></div>
    </div>
</div>
