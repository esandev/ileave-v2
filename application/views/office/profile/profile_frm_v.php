<?php $this->view('ajax_loading_v'); ?>

<?php if (isset($msg_text)): ?>
    <script>
        $(function() {

            $.msgGrowl({
                type: 'success'
                , title: 'Message'
                , text: '<?php echo $msg_text; ?>'
                , position: 'top-center'

            });

        });
    </script>
<?php endif; ?>

<script type="text/javascript">
    $(function() {
        $('#alert_frm').css('display', 'none');
        $('#birthday').mask("99/99/9999");
        $('#date_serve').mask("99/99/9999");
        $("#birthday").datepicker({dateFormat: 'dd/mm/yy'});
        $("#date_serve").datepicker({dateFormat: 'dd/mm/yy'});
        $("#myform").validate({
            onkeyup: false,
            messages: {
                password2: {
                    equalTo: "กรอกรหัสผ่านให้ตรงกัน"
                }
            }
        });

        $.metadata.setType("attr", "validate");
    });

    function save() {
        $('#myform').submit();
        return false;
    }
</script>


<div class="container">
    <div id="control" class="widget widget-form">
        <div class="widget-header">
            <h3>	      					
                <i class="icon-user-md"></i>
                <?php echo $page_title; ?>      					
            </h3>	
        </div> <!-- /widget-header -->


        <div class="widget-content">
            <div class="alert alert-error" style="display:none" id="alert_frm"></div>
            <?php echo form_open_multipart('profile/save', array('id' => 'myform', 'class' => 'form-horizontal')); ?>
            <input type="hidden" id="id" name="id" value="<?php echo @$row['id']; ?>"/>
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">ประเภทผู้ใช้งาน</label>
                    <div class="controls">
                        <?php
                        switch ($row['mem_type']) {
                            case '0':
                                echo "ผู้ลา";
                                break;
                            case '1':
                                echo "ผู้ดูแลระบบ";
                                break;
                            case '2':
                                echo "ผู้อนุมัติประจำแผนก";
                                break;
                        }
                        ?>
                        &nbsp;&nbsp;
                        <strong>สถานะ :</strong>
                        <?php
                        switch ($row['status']) {
                            case '0':
                                echo "รออนุมัติ";
                                break;
                            case '1':
                                echo "ใช้งาน";
                                break;
                            case '2':
                                echo "ระงับการใช้งาน";
                                break;
                        }
                        ?>
                    </div>
                </div>
                <?php if(@$row['picture']==''){ ?>
<div class="control-group">
                    <label class="control-label" for="focusedInput">รูป :<span class="required">*</span></label>
                    <div class="controls">
                      <input type="file" name="pic_file" size="80" value="" />
            <span class="required">* ไฟล์ jpg,gif,png ขนาดที่แนะนำคือ 100 x 100 pixel</span>
                    </div>
                </div>
                <?php }else{ ?>
                <div class="control-group">
                    <label class="control-label" for="focusedInput"><img src="<?php echo base_url();?>assets/upload/member/<?php echo @$row['picture'];?>" width ="100px" height ="100px"/></label>
                    <div class="controls">
                    <input type="file" name="pic_file" size="80" value="" />
            <span class="required">* ไฟล์ jpg,gif,png ขนาดที่แนะนำคือ 100 x 100 pixel</span>
                      
                    </div>
                </div>
                
                <?php } ?>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">ชื่อ-นามสกุล :<span class="required">*</span></label>
                    <div class="controls">
                        <input type="text" class="txt required" name="name" style="width: 350px" value="<?php echo @$row['name']; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">วัน/เดือน/ปี เกิด :</label>
                    <div class="controls">
                        <input type="text" class="txt" id = "birthday" name="birthday" style="width: 80px" value="<?php echo $this->mydate->dateToText(@$row['birthday']); ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">ชื่อเข้าใช้งาน :<span class="required">*</span></label>
                    <div class="controls">
                        <?php echo @$row['username']; ?>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="focusedInput">รหัสผ่าน :</label>
                    <div class="controls">
                        <input type="text" class="txt" id="password1" name="password1" value="" /> <span class="label label-info">ว่างไว้หากไม่ต้องการเปลี่ยน</span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">ยืนยันรหัสผ่าน :</label>
                    <div class="controls">
                        <input type="text" class="txt" id="password2" name="password2" equalTo="#password1" value="" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">ตำแหน่ง :<span class="required">*</span></label>
                    <div class="controls">
                        <input type="text" class="txt required" name="position" style="width: 350px" value="<?php echo @$row['position']; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">สังกัดแผนก :<span class="required">*</span></label>
                    <div class="controls">
                        <select id="depart_id" name="depart_id" style="padding: 2px;" title="เลือกแผนก !" validate="required:true">
                            <option value="">*** เลือกแผนก ***</option>
                            <?php
                            foreach ($depart as $r):
                                $selected = ($r['id'] == @$row['depart_id']) ? 'selected' : '';
                                ?>

                                <option value="<?php echo $r['id']; ?>" <?php echo $selected; ?>><?php echo $r['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">โทรศัพท์(มือถือ) :</label>
                    <div class="controls">
                        <input type="text" class="txt" name="mobile" value="<?php echo @$row['mobile']; ?>" />&nbsp;&nbsp; เบอร์ติดต่อภายใน
                        <input type="text" class="txt" name="tel" value="<?php echo @$row['tel']; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">อีเมลล์ :</label>
                    <div class="controls">
                        <input type="text" class="txt" name="email" value="<?php echo @$row['email']; ?>" style="width:350px"/>
                    </div>
                </div>

                <div class="form-actions">

                    <a href="#" class="btn btn-Warning btn-large" onclick="return save();"><i class="icon-hdd"></i> บันทึกข้อมูล</a>
                    <?php echo anchor('main', "<i class='icon-remove-circle'></i> ปิด", array('class' => 'btn btn-danger btn-large')); ?>
                </div>
            </fieldset>

            <?php echo form_close(); ?>

        </div>
    </div>

</div>
