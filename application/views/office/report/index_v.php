<?php 
   $s_login = $this->session->userdata('s_login');
   $member_id = $s_login['login_id'];
?>
<!-- Styles -->
<link href="<?php echo base_url();?>assets/css/pages/reports.css" rel="stylesheet">

 <script type="text/javascript" src="<?php echo base_url();?>assets/js/highchart/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/highchart/exporting.js"></script>
<script type="text/javascript">
    $(function() {
    	  $('#datefrom').mask("99/99/9999");
        $('#dateto').mask("99/99/9999");
        $("#datefrom").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#dateto").datepicker({ dateFormat: 'dd/mm/yy' });
        load_div_grid();
        load_div_grid_pie();
        
    });

    function load_div_grid() {
        var p = {};
        $('#div_chart').load("<?php echo site_url('report/ajax_get_chart') ?>", p);

        return false;
    }
  function load_div_grid_pie() {
        var p = {};

         $('#pie_chart').load("<?php echo site_url('report/ajax_get_chart_pie') ?>", p);
        return false;
    }
        
    function preview(){
        var ret = '';
        if($('#datefrom').val()==''){
            ret+='กรอกข้อมูล จากวันที่ \n';
        }
        if($('#dateto').val()==''){
            ret+='กรอกข้อมูล วันที่ถึง \n';
        }
        
        if(ret==''){
            $('#myform').submit();
        }else{
            alert(ret);
        }
        
        return false;
    }
</script>
</script>
<div class="container">
		
				
		<div class="row">
			
			<div class="span12">
	      		
	      		<div id="big-stats-container" class="widget">
	      			<div class="widget-header">	  
	      				แสดงรายการอนุมัติแล้วเท่านั้น
	      				</div>
	      			<div class="widget-content">
	      				
	      				<div id="big_stats" class="cf">
	      				    <?php
	      				 
                    $sql = "SELECT * ,COUNT(leave_type_id) AS total_leave FROM tbleavemanage WHERE member_id = '$member_id' AND status = 'approve' GROUP BY leave_type_id";
                    $leave = $this->db->query($sql)->result_array();
                    foreach ($leave as $rs):
                       
                            $leavetype = $this->db->get_where('tbleave_type', array('id' => $rs['leave_type_id']))->result_array();
                            foreach ($leavetype as $r):
                                ?>
							<div class="stat">								
								<h4> <?php echo $r['name']; ?></h4>
								<span class="value"> <?Php
                                        echo $rs['total_leave'] . " ครั้ง";
                                        ?></span>								
							</div> <!-- .stat -->
							<?php endforeach; ?>
<?php endforeach; ?>

												</div>
			      		
		      		</div> <!-- /widget-content -->
		      		
	      		</div> <!-- /widget -->
	      		
      		</div> <!-- /span12 -->
      		
      		
      		
      		<div class="span12">
	      		
	      		<div class="widget">
	      			
	      			<div class="widget-header">	     
	      			 <div class="widget-actions">
                <?php echo form_open('report/preview/', array('class' => 'form-inline','id' => 'myform', 'target' => '_blank')); ?>
<input type="hidden" id="savetype" name="savetype" value=""/>
                <div class="input-append">
                 <select id="leave_type_id" name="leave_type_id" style="padding: 2px;" title="เลือกเครื่องมือวิจัย">
                <option value="">*** ทุกรายการ ***</option>
                <?php foreach($row_leave as $r):
                    $selected = ($r['id']==@$row['leave_type_id']) ? 'selected' : '';
                    ?>
                <option value="<?php echo $r['id'];?>" <?php echo $selected; ?>><?php echo $r['name'];?></option>
                <?php endforeach; ?>
            </select>
                                        <a href="#" onclick="return preview();" class="btn"><i class='icon-print'></i> แสดงรายงาน</a>
                </div>
                <?php echo form_close(); ?>
            </div> <!-- /.widget-actions -->		 				
	      				<h3>
	      					<i class="icon-bar-chart"></i>
	      					Bar Chart แสดงทั้งหมด
      					</h3>	      				
      				</div> <!-- /widget-header -->
	      			
	      			<div class="widget-content">
	      				
			      		<div id="div_chart" class="chart-holder"></div> <!-- /area-chart -->	
			      		
		      		</div> <!-- /widget-content -->
		      		
	      		</div> <!-- /widget -->
	      		
      		</div> <!-- /span12 -->      		
      		
			
		</div> <!-- /row -->
		
		
		
		<div class="row">
		
      	      		
      		
      		
      		
      		<div class="span12">
      		
      		<div class="widget widget-table">
						
					<div class="widget-header">						
						<h3>
							<i class="icon-th-list"></i>
							รายการลาแต่ละประเภท							
						</h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						<?php
							$this->db->order_by('id','asc');
							$type = $this->db->get('tbleave_type')->result_array();
							foreach($type as $rs_type):
						?>
						
						<table class="table table-striped table-bordered responsive">
							<thead>
							
							<tr>
									<th>ประเภทการลา : </th>
									<th colspan="4"><b><?php echo $rs_type['name'];?></b></th>
									
							</tr>
								<tr>
									<th>ลำดับที่</th>
									<th>วันที่ลา</th>
									<th>จำนวนวันลา(วัน)</th>
									<th>วันที่ทำรายการ</th>
									<th>สถานะ</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$this->db->order_by('id','desc');
								$list = $this->db->get_where('tbleavemanage',array('leave_type_id'=>$rs_type['id'],'member_id'=>$member_id))->result_array();
								$i = 1;
								foreach($list as $rs_list):
								?>
								<tr class="odd gradeX">
									<td><?php echo $i++;?> </td>
									<td><?php echo $this->mydate->dateToText($rs_list['datefrom']) . ' - ' . $this->mydate->dateToText($rs_list['dateto']);?></td>
									<td><?php echo $rs_list['amountdate'];?></td>
									<td class="center"><?php echo $this->mydate->dateToText($rs_list['dateregist']);?></td>
									<td class="center">
									<?php echo $rs_list['status'];?></td>
								</tr>
							  <?php endforeach; ?>					
							</tbody>
						</table>
						 <?php endforeach; ?>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->	
				
		    </div> <!-- /span7 -->
      		 		
		</div> <!-- /row -->
		
		
	</div> <!-- /.container -->