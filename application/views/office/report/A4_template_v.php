<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo @$rpt_name;?></title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/reset.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/font.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/print_preview.css"  media="all"/>
    </head>
    <body>
        <div id="a4">
            <?php
            echo $tbl;
            ?>
            <p class="foot_br">&nbsp;</p>
        </div>
    </body>
</html>
