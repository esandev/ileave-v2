<?php if (isset($msg_text)): ?>
    <script>
        $(function() {

            $.msgGrowl({
                type: '<?php echo $type_title;?>'
                , title: 'Message'
                , text: '<?php echo $msg_text; ?>'
                , position: 'top-center'

            });

        });
    </script>

<?php endif; ?>

<script type="text/javascript">
    $(function() {
        $("#myform").validate({
            onkeyup: false,
            messages: {
                con_pass: {
                    equalTo: "กรอกรหัสผ่านให้ตรงกัน"
                }
            }
        });
    });

    function onSubmit() {
        $('#myform').submit();
        return false;
    }
</script>

<div class ="container">

    <div id="control" class="widget widget-form">

        <div class="widget-header">	      				
            <h3>
                <i class="icon-pencil"></i>
                เปลี่ยนรหัสผ่าน	      					
            </h3>	
        </div> <!-- /widget-header -->

        <div class="widget-content">

            <?php echo form_open('member/changePassSave', array('id' => 'myform', 'autocomplete' => 'off', 'class' => 'form-horizontol')); ?>
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="input01">รหัสผ่านปัจุบัน <span class="required">*</span></label>
                    <div class="controls">
                        <input type="password" class="txt required" id="old_pass" name="old_pass" value="" maxlength="20"/>

                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="input01">รหัสผ่านใหม่ <span class="required">*</span></label>
                    <div class="controls">
                        <input type="password" class="txt required" id="new_pass" name="new_pass" value="" maxlength="20"/>

                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="input01">ยืนยันอีกครั้ง <span class="required">*</span></label>
                    <div class="controls">
                        <input type="password" class="txt required" id="con_pass" name="con_pass" equalTo="#new_pass" value="" maxlength="20"/>

                    </div>
                </div>

                <div class="form-actions">
                    <a href="#" class="btn btn-large btn-info" onclick="return onSubmit();">

                        บันทึกการเปลี่ยนแปลง
                    </a>

                </div>
            </fieldset>
            <?php echo form_close(); ?>	

        </div> <!-- /widget-content -->
    </div>
</div>

