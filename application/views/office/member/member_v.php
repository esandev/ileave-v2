<?php $this->view('ajax_loading_v'); ?>
<script type="text/javascript">
    $(function() {
        load_div_grid();

        $("div.pagination a").live("click", function(event) {
            var p = {};
            p['txtsearch'] = $('#txtsearch').val();

            event.preventDefault();
            var url = $(this).attr("href");
            if (url != undefined) {
                $('#div_grid').load(url, p);
            }
            return false;
        });


        $('#txtsearch').focus();
        $('#txtsearch').keydown(function(e) {
            if ((e.keyCode == 13)) {
                load_div_grid();
                return false;
            }
        });

        $("#myform").validate({
            submitHandler: function() {
                save();
            }
        });
    });

    function load_div_grid() {
        var p = {};
        p['txtsearch'] = $('#txtsearch').val();
        $('#div_grid').load("<?php echo site_url('member/ajax_get_grid') ?>", p);
        return false;
    }

    function grid_btn_del(id) {
        if (confirm('คุณต้องการลบข้อมูลรายการนี้ ใช่หรือไม่?')) {
            var p = {};
            p['id'] = id;
            $.ajax({
                data: p,
                url: "<?php echo site_url('member/ajax_delete') ?>",
                type: 'POST',
                dataType: 'json',
                success: function() {
                    load_div_grid();
                },
                error: function() {
                    alert('ไม่สามารถทำรายการได้ !!');
                }
            });
        }
        return false;
    }
</script>
<div class="container">
    
       <div class="widget-header">
        <h3>
            <i class="icon-th-list"></i> 
            <?php echo $page_title; ?>

        </h3>
            
               
                <div class="pull-right">
                    <?php echo anchor('member/news', "<i class='icon-plus'></i> เพิ่มข้อมูล", array('class' => 'btn')); ?>
                    <?php echo anchor('main', "<i class='icon-remove-circle'></i> ปิด", array('class' => 'btn')); ?>
                </div>
       </div>
           <div class="textbox_content">
        <div class="widget-header">

            <div class="widget-actions">
                <?php echo form_open('', array('class' => 'form-inline')); ?>

                <div class="input-append">
                    <input placeholder="ค้นหารายการ" onclick="return load_div_grid();"class="span4" id="txtsearch" name="txtsearch" type="text">

                </div>
                <?php echo form_close(); ?>
            </div> <!-- /.widget-actions -->		
        </div> <!-- /.widget-header -->

        <div id="div_grid"></div>
    </div>
        </div>
    </div>  


