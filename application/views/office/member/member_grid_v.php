
<div class="widget widget-table" >

    <div class="widget-content">

        <table class="table table-bordered table-striped " id="">
           <thead>
        <tr>
            <th width="80">No.</th>
            <th>ชื่อ-สกุล</th>
            <th width="140">ชื่อเข้าใช้งาน</th>
            <th width="120">ประเภท</th>
            <th width="80">สถานะ</th>
            <th width="120">เมนู</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $irow = 0;
        $i = $this->uri->segment(3)+1;
        foreach ($query as $r) {
            $irow++;
            $id=$r['id'];
            
            echo "<tr>";
            echo "<td>".$i++."</td>";
            echo "<td>".$r['name']."</td>";
            echo "<td>".$r['username']."</td>";
            echo "<td class='center'>".$member_m->display_memtype($r['mem_type'])."</td>";
            echo "<td class='center'>".$member_m->display_status($r['status'])."</td>";
            echo "<td class='center'>";
            echo anchor("member/edit/$id",'<i class="icon-pencil"></i>แก้ไข',array('class'=>'btn btn-mini')).  nbs();
            echo '<a href="#" onclick="return grid_btn_del(\''.$id.'\')" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i>ลบ</a>';
            echo "</td>";
            echo "</tr>";
        }
        if($irow==0){
            echo "<tr><td colspan='5' class='center'>*** ไม่พบข้อมูล ***</td></tr>";
        }
        ?>
    </tbody>
</table>
<?php echo $pagination; ?>
    </div>