$(function () {
    $('ul#my_nav .my_collapse').parents('li').find('ul').hide();
    $('ul#my_nav .my_collapse').toggle(function () {
        $(this).parents('li').find('ul').stop(true, true).slideDown('fast');
        return false;
    }, function () {
        $(this).parents('li').find('ul').stop(true, true).slideUp('fast');
        return false;
    });
   
    $('ul#my_nav a.active').parents('li').find('ul').stop(true, true).slideDown('fast');
    $("a.my_collapse").click(function(){
        $("a.my_collapse.active").removeClass("active");
        $(this).addClass("active");
    });
});

function getBaseURL() {
    var url = location.href;  // entire url including querystring - also: window.location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));
    var url = location.href;  // window.location.href;
    var pathname = location.pathname;  // window.location.pathname;
    var index1 = url.indexOf(pathname);
    var index2 = url.indexOf("/", index1 + 1);
    var baseLocalUrl = url.substr(0, index2);

    var strMessage = baseLocalUrl + "/";
    return strMessage.replace( /index.php/i , '' );
}

function loadMsg(type, message, timeOut) {
    $('.loadmsg').remove();
    if (!timeOut) {
        timeOut = 3000;
    }
    var timeId = new Date().getTime();
    if (type != '' && message != '') {
        $('<div class="loadmsg ' + type + '" id="msg_' + timeId + '"><img src="'+getBaseURL()+'assets/images/msg_' + type + '.png" alt="" />' + message + '</div>').hide().appendTo('body').fadeIn();
        var timer = setTimeout(function () {
            $('.loadmsg#msg_' + timeId + '').fadeOut('slow', function () {
                $(this).remove();
            });
        }, timeOut);
    }
}
if (!$('#my_content').hasClass('loginbox')) {
    loadMsg('success', 'โหลดหน้านี้เสร็จสมบูรณ์');
}